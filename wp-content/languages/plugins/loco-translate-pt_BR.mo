��          �       �      �     �     �     �  '     5   .     d     x  #   �     �     �     �     �     	          5     D     \  '   r     �     �     �     �     �     �     �     �  8     �  ;  
   �     
       3   +  :   _     �     �  .   �     �               5     K     ]     }  #   �  
   �  /   �     �          %     5     L     [     a     n  ?   �   %s fuzzy %s untranslated %s%% translated 1 new string added %s new strings added 1 obsolete string removed %s obsolete strings removed 1 string %s strings Already up to date with %s Already up to date with source code Compiling MO files Filter translations Generate hash tables Include Fuzzy strings Loco Translate Loco, Translation Management Merged from %s Merged from source code Modified timeUpdated Number of backups to keep of each file: Permission denied Save settings Settings Settings saved Template file Themes Tim Whitlock Unknown error Your changes will be lost if you continue without saving PO-Revision-Date: 2018-11-15 11:45+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Loco https://localise.biz/
Language: pt_BR
Project-Id-Version: Plugins - Loco Translate - Stable (latest release)
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-07-23 09:58+0000
Last-Translator: rodrigoa <rodrigoadamski@gmail.com>
Language-Team: Português do Brasil %s incerto %s não traduzido %s%% traduzido 1 novo termo adicionado %s novos termos adicionados 1 um termo obsoleto removido %s termos obsoletos removidos 1 termo %s termos Atualizado desde %s Já está atualizado a partir do código fonte Compilando arquivos MO Filtrar traduções Gerar tabela de dispersão Incluir Fuzzy strings Tradução (Loco) Loco, Gerenciador de Tradução Combinado a partir de %s Combinado a partir do código fonte Atualizado Número de backups para manter de cada arquivo: Permissão negada Configurações de salvamento Configurações Configurações salvas Arquivo modelo Temas Tim Whitlock Erro desconhecido Suas alterações serão perdidas se você continuar sem salvar 