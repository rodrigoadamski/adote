<?php
/**
 * The header for our theme.
 *
 * @package Careerfy
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
        <?php
        wp_head();
        ?>
    </head>

    <body <?php body_class(); ?>>

        <!--// Main Wrapper \\-->
        <div class="careerfy-wrapper">
            <?php
            $careerfy__options = careerfy_framework_options();
            $careerfy_loader = isset($careerfy__options['careerfy-site-loader']) ? $careerfy__options['careerfy-site-loader'] : '';
            if ($careerfy_loader == 'on') {
                ?>
                <div class="careerfy-loading-section"><div class="line-scale-pulse-out"><div></div><div></div><div></div><div></div><div></div></div></div>
                <?php
            }
            ?>
            <!--// Header \\-->
            <header id="careerfy-header" class="<?php echo careerfy_header_class() ?>">

                <!--// MainHeader \\-->
                <?php
                // header section
                do_action('careerfy_header_section');
                ?>  
                <!--// MainHeader \\-->

            </header>
            <!--// Header \\-->
            <?php do_action('careerfy_header_breadcrumbs'); ?>
