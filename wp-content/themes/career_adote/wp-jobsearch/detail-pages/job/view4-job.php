<?php
wp_enqueue_style('careerfy-job-detail-four');
wp_enqueue_script('careerfy-countdown');
wp_enqueue_script('jobsearch-addthis');
global $post, $jobsearch_plugin_options;
$job_id = $post->ID;

$job_employer_id = get_post_meta($post->ID, 'jobsearch_field_job_posted_by', true); // get job employer
wp_enqueue_script('jobsearch-job-functions-script');
$employer_cover_image_src_style_str = '';
if ($job_employer_id != '') {
    if (class_exists('JobSearchMultiPostThumbnails')) {
        $employer_cover_image_src = JobSearchMultiPostThumbnails::get_post_thumbnail_url('employer', 'cover-image', $job_employer_id);
        if ($employer_cover_image_src != '') {
            $employer_cover_image_src_style_str = ' style="background:url(' . esc_url($employer_cover_image_src) . ');background-size: cover;"';
        }
    }
}
$all_location_allow = isset($jobsearch_plugin_options['all_location_allow']) ? $jobsearch_plugin_options['all_location_allow'] : '';
$job_views_publish_date = isset($jobsearch_plugin_options['job_views_publish_date']) ? $jobsearch_plugin_options['job_views_publish_date'] : '';

while (have_posts()) : the_post();
    $post_id = $post->ID;

    $rand_num = rand(1000000, 99999999);
    $post_thumbnail_id = jobsearch_job_get_profile_image($post_id);
    $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'jobsearch-job-medium');
    $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';
    $post_thumbnail_src = $post_thumbnail_src == '' ? jobsearch_no_image_placeholder() : $post_thumbnail_src;
    $application_deadline = get_post_meta($post_id, 'jobsearch_field_job_application_deadline_date', true);
    $jobsearch_job_posted = get_post_meta($post_id, 'jobsearch_field_job_publish_date', true);

    $job_max_salary = get_post_meta($post_id, 'jobsearch_field_job_max_salary', true);
    $job_salary_sep = get_post_meta($post_id, 'jobsearch_field_job_salary_sep', true);
    $job_salary_deci = get_post_meta($post_id, 'jobsearch_field_job_salary_deci', true);
   //$job_max_salary = number_format((int) $job_max_salary, $job_salary_deci, "//.", $job_salary_sep);



    $jobsearch_job_posted_ago = jobsearch_time_elapsed_string($jobsearch_job_posted, ' ' . esc_html__('posted', 'wp-jobsearch') . ' ');
    $jobsearch_job_posted_formated = '';
    if ($jobsearch_job_posted != '') {
        $jobsearch_job_posted_formated = date_i18n(get_option('date_format'), ($jobsearch_job_posted));
    }
    $get_job_location = get_post_meta($post_id, 'jobsearch_field_location_address', true);
    $job_city_title = '';
    $get_job_city = get_post_meta($post_id, 'jobsearch_field_location_location3', true);
    if ($get_job_city == '') {
        $get_job_city = get_post_meta($post_id, 'jobsearch_field_location_location2', true);
    }
    if ($get_job_city != '') {
        $get_job_country = get_post_meta($post_id, 'jobsearch_field_location_location1', true);
    }
    $job_city_tax = $get_job_city != '' ? get_term_by('slug', $get_job_city, 'job-location') : '';
    if (is_object($job_city_tax)) {
        $job_city_title = isset($job_city_tax->name) ? $job_city_tax->name : '';

        $job_country_tax = $get_job_country != '' ? get_term_by('slug', $get_job_country, 'job-location') : '';
        if (is_object($job_country_tax)) {
            $job_city_title .= isset($job_country_tax->name) ? ', ' . $job_country_tax->name : '';
        }
    } else if ($job_city_title == '') {
        $get_job_country = get_post_meta($post_id, 'jobsearch_field_location_location1', true);
        $job_country_tax = $get_job_country != '' ? get_term_by('slug', $get_job_country, 'job-location') : '';
        if (is_object($job_country_tax)) {
            $job_city_title .= isset($job_country_tax->name) ? $job_country_tax->name : '';
        }
    }
    if ($job_city_title != '' && $get_job_location == '') {
        $get_job_location = $job_city_title;
    }
    $sectors_enable_switch = isset($jobsearch_plugin_options['sectors_onoff_switch']) ? $jobsearch_plugin_options['sectors_onoff_switch'] : '';
    $job_date = get_post_meta($post_id, 'jobsearch_field_job_date', true);
    $job_views_count = get_post_meta($post_id, 'jobsearch_job_views_count', true);
    $job_type_str = jobsearch_job_get_all_jobtypes($post_id, 'careerfy-jobdetail-four-list-status', '', '', '', '', 'span');
    $sector_str = jobsearch_job_get_all_sectors($post_id, '', ' ' . esc_html__('in', 'wp-jobsearch') . ' ', '', '<small class="post-in-category">', '</small>');
    $company_name = jobsearch_job_get_company_name($post_id, '');
    $skills_list = jobsearch_job_get_all_skills($post_id);
    $job_obj = get_post($post_id);
    $job_content = isset($job_obj->post_content) ? $job_obj->post_content : '';
    $job_content = apply_filters('the_content', $job_content);
    $job_salary = jobsearch_job_offered_salary($post_id);
    $job_applicants_list = get_post_meta($post_id, 'jobsearch_job_applicants_list', true);
    $job_applicants_list = jobsearch_is_post_ids_array($job_applicants_list, 'candidate');
    if (empty($job_applicants_list)) {
        $job_applicants_list = array();
    }
    $job_applicants_count = !empty($job_applicants_list) ? count($job_applicants_list) : 0;

    $current_date = strtotime(current_time('d-m-Y H:i:s'));
    ?>  

    <div class="careerfy-jobdetail-four-list" <?php echo ($employer_cover_image_src_style_str); ?>>
        <span class="careerfy-jobdetail-four-transparent"></span>
        <div class="container">
            <div class="row">
                <div class="careerfy-column-12">
                    <img src="extra-images/jobdetail-four-logo.jpg" alt="">

                    <?php if ($post_thumbnail_src != '') { ?>
                        <figure><a href="#"><img src="<?php echo esc_url($post_thumbnail_src) ?>" alt=""></a></figure>
                    <?php }
                    ?>
                    <div class="careerfy-jobdetail-four-list-text">
                        <?php
                        if ($job_type_str != '') {
                            ?>
                            
                                <?php
                                echo force_balance_tags($job_type_str);
                                ?>
                            
                            <?php
                        }
                        ?>
                        
                        <h1><?php echo get_the_title($post_id); ?></h1>
                        <ul class="careerfy-jobdetail-four-options">

                            <?php
                            if (!empty($get_job_location) && $all_location_allow == 'on') {
                                $google_mapurl = 'https://www.google.com/maps/search/' . $get_job_location;
                                ?>
                                <li><i class="fa fa-map-marker"></i> <?php echo esc_html($get_job_location); ?> <a href="<?php echo esc_url($google_mapurl); ?>" target="_blank" class="careerfy-jobdetail-view"><?php echo esc_html__('View on Map', 'wp-jobsearch') ?></a></li>
                                <?php
                            }

                            if (!empty($company_name) || !empty($jobsearch_job_posted_ago)) {
                                echo '<li>';
                                if ($company_name != '') {
                                    echo '<i class="careerfy-icon careerfy-building"></i>' . force_balance_tags($company_name);
                                }
                                if ($jobsearch_job_posted_ago != '' && $job_views_publish_date == 'on') {
                                    ?>
                                    <small><?php echo esc_html($jobsearch_job_posted_ago); ?></small>
                                    <?php
                                }
                                echo '</li>';
                            }
                            $jobsearch_last_date_formated = '';
                            if ($application_deadline != '') {
                                $jobsearch_last_date_formated = date_i18n(get_option('date_format'), ($application_deadline));
                            }
                            ?>
                            <li><a href="#"><i class="careerfy-icon careerfy-view"></i> <?php echo esc_html__('Visitas ', 'careerfy'); ?><?php echo absint($job_views_count); ?></a></li>
                            <?php
                            if (isset($jobsearch_job_posted_formated) && !empty($jobsearch_job_posted_formated)) {
                                ?><li><i class="careerfy-icon careerfy-calendar"></i> <?php echo esc_html__('Posted Date ', 'careerfy'); ?>: <?php echo esc_html($jobsearch_job_posted_formated); ?></li><?php
                                }
                                if (isset($jobsearch_last_date_formated) && !empty($jobsearch_last_date_formated)) {
                                    ?><li><i class="careerfy-icon careerfy-calendar"></i> <?php echo esc_html__('Inscrições até ', 'careerfy'); ?>: <?php echo esc_html($jobsearch_last_date_formated); ?></li><?php
                                }
                                ?>   
                        </ul>
                        <?php
                        $popup_args = array(
                            'job_id' => $job_id,
                            'btn_class' => 'careerfy-jobdetail-four-btn',
                        );
                        do_action('jobsearch_job_send_to_email_filter', $popup_args);
                        ?>
                        <ul class="careerfy-jobdetail-four-media">
                            <li><span><?php echo esc_html__('Share this Job ', 'careerfy') ?>:</span></li>
                            <li><a href="javascript:void(0);" data-original-title="twitter" class="fa fa-twitter addthis_button_twitter"></a></li>
                            <li><a href="javascript:void(0);" data-original-title="facebook" class="fa fa-facebook-f addthis_button_facebook"></a></li>
                            <li><a href="javascript:void(0);" data-original-title="linkedin" class="fa fa-linkedin addthis_button_linkedin"></a></li>
                            <li><a href="javascript:void(0);" data-original-title="share_more" class="jobsearch-icon jobsearch-plus addthis_button_compact"></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- JobDetail SubHeader Main List -->

    <!-- Main Content -->
    <div class="careerfy-main-content">

        <!-- Main Section -->
        <div class="careerfy-main-section">
            <div class="container">
                <div class="row">

                    <!-- Job Detail Content -->
                    <div class="careerfy-column-8">
                        <div class="careerfy-typo-wrap">
                            <div class="careerfy-jobdetail-content">
                                <?php
                                if ($job_content != '') {
                                    ob_start();
                                    ?>
                                    <div class="careerfy-content-title"><h2><?php echo esc_html__('Job Description', 'wp-jobsearch') ?></h2></div>
                                    <div class="jobsearch-description">
                                        <?php
                                        echo force_balance_tags($job_content);
                                        ?>
                                    </div>
                                    <?php
                                    $job_det_output = ob_get_clean();
                                    echo apply_filters('jobsearch_job_detail_content_detail', $job_det_output, $job_id);
                                    $job_attachments_switch = isset($jobsearch_plugin_options['job_attachments']) ? $jobsearch_plugin_options['job_attachments'] : '';
                                    if ($job_attachments_switch == 'on') {
                                        $all_attach_files = get_post_meta($job_id, 'jobsearch_field_job_attachment_files', true);
                                        if (!empty($all_attach_files)) {
                                            ?>
                                            <div class="jobsearch-content-title"><h2><?php esc_html_e('Attached Files', 'wp-jobsearch') ?></h2></div>
                                            <div class="jobsearch-file-attach-sec">
                                                <ul class="jobsearch-row">
                                                    <?php
                                                    foreach ($all_attach_files as $_attach_file) {
                                                        $_attach_id = jobsearch_get_attachment_id_from_url($_attach_file);
                                                        $_attach_post = get_post($_attach_id);
                                                        $_attach_mime = isset($_attach_post->post_mime_type) ? $_attach_post->post_mime_type : '';
                                                        $_attach_guide = isset($_attach_post->guid) ? $_attach_post->guid : '';
                                                        $attach_name = basename($_attach_guide);

                                                        $file_icon = 'fa fa-file-text-o';
                                                        if ($_attach_mime == 'image/png' || $_attach_mime == 'image/jpeg') {
                                                            $file_icon = 'fa fa-file-image-o';
                                                        } else if ($_attach_mime == 'application/msword' || $_attach_mime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
                                                            $file_icon = 'fa fa-file-word-o';
                                                        } else if ($_attach_mime == 'application/vnd.ms-excel' || $_attach_mime == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
                                                            $file_icon = 'fa fa-file-excel-o';
                                                        } else if ($_attach_mime == 'application/pdf') {
                                                            $file_icon = 'fa fa-file-pdf-o';
                                                        }
                                                        ?>
                                                        <li class="jobsearch-column-4">
                                                            <div class="file-container">
                                                                <a href="<?php echo ($_attach_file) ?>" download="<?php echo ($attach_name) ?>" class="file-download-icon"><i class="<?php echo ($file_icon) ?>"></i> <?php echo ($attach_name) ?></a>
                                                                <a href="<?php echo ($_attach_file) ?>" download="<?php echo ($attach_name) ?>" class="file-download-btn"><?php esc_html_e('Download', 'wp-jobsearch') ?> <i class="jobsearch-icon jobsearch-download-arrow"></i></a>
                                                            </div>
                                                        </li>
                                                        <?php
                                                    }
                                                    ?>
                                                </ul>
                                            </div>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                <div class="careerfy-jobdetail-tags-style2">
                                    <?php echo force_balance_tags($skills_list); ?>
                                </div>
                                <div class="careerfy-jobdetail-four-links">
                                    <?php
                                    // wrap in this this due to enquire arrange button style.
                                    $before_label = esc_html__('Shortlist', 'wp-jobsearch');
                                    $after_label = esc_html__('Shortlisted', 'wp-jobsearch');
                                    $book_mark_args = array(
                                        'before_label' => $before_label,
                                        'after_label' => $after_label,
                                        'before_icon' => 'careerfy-icon careerfy-heart',
                                        'after_icon' => 'fa fa-heart',
                                        'anchor_class' => 'widget_jobdetail_three_apply_btn',
                                        'view' => 'job_detail_3',
                                        'job_id' => $job_id,
                                    );
                                    do_action('jobsearch_job_shortlist_button_frontend', $book_mark_args);
                                    if ($application_deadline != '' && $application_deadline <= $current_date) {
                                        ?>
                                        <span class="deadline-closed"><?php esc_html_e('closed.', 'wp-jobsearch'); ?></span>
                                        <?php
                                    } else {
                                        $arg = array(
                                            'classes' => 'color1',
                                            'btn_before_label' => esc_html__('Aplicar a esse projeto', 'wp-jobsearch'),
                                            'btn_after_label' => esc_html__('Successfully Applied', 'wp-jobsearch'),
                                            'btn_applied_label' => esc_html__('Applied', 'wp-jobsearch'),
                                            'job_id' => $job_id
                                        );
                                        $apply_filter_btn = apply_filters('jobsearch_job_applications_btn', '', $arg);
                                        echo force_balance_tags($apply_filter_btn) . '';
                                    }
                                    $facebook_login = isset($jobsearch_plugin_options['facebook-social-login']) ? $jobsearch_plugin_options['facebook-social-login'] : '';
                                    $linkedin_login = isset($jobsearch_plugin_options['linkedin-social-login']) ? $jobsearch_plugin_options['linkedin-social-login'] : '';
                                    $google_social_login = isset($jobsearch_plugin_options['google-social-login']) ? $jobsearch_plugin_options['google-social-login'] : '';
                                    $apply_social_platforms = isset($jobsearch_plugin_options['apply_social_platforms']) ? $jobsearch_plugin_options['apply_social_platforms'] : '';
                                    if (!is_user_logged_in() && ($facebook_login == 'on' || $linkedin_login == 'on' || $google_social_login == 'on') && !empty($apply_social_platforms)) {
                                        $apply_args_fb = array(
                                            'job_id' => $job_id,
                                            'classes' => 'color2',
                                            'view' => 'job4',
                                        );
                                        $apply_args_linkdin = array(
                                            'job_id' => $job_id,
                                            'classes' => 'color3',
                                            'view' => 'job4',
                                        );
                                        $apply_args_google = array(
                                            'job_id' => $job_id,
                                            'classes' => 'color4',
                                            'view' => 'job4',
                                        );
                                        
                                        if (in_array('facebook', $apply_social_platforms)) {
                                            do_action('jobsearch_apply_job_with_fb', $apply_args_fb);
                                        }
                                        if (in_array('linkedin', $apply_social_platforms)) {
                                            do_action('jobsearch_apply_job_with_linkedin', $apply_args_linkdin);
                                        }
                                        if (in_array('google', $apply_social_platforms)) {
                                            do_action('jobsearch_apply_job_with_google', $apply_args_google);
                                        }
                                        ?>
                                        <span class="apply-msg" style="display: none;"></span>
                                        <?php
                                    }
                                    ?>    
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Job Detail Content -->
                    <!-- Job Detail SideBar -->
                    <aside class="careerfy-column-4">
                        <div class="careerfy-typo-wrap">
                            <!-- Widget Detail Services -->
                            <?php
                            ob_start();
                            $cus_fields = array('content' => '');
                            $cus_fields = apply_filters('jobsearch_custom_fields_list', 'job', $job_id, $cus_fields, '<li>', '</li>', '', true, true, true, 'careerfy');
                            if (isset($cus_fields['content']) && $cus_fields['content'] != '') {
                                ?>
                                <div class="widget careerfy-candidatedetail-services">
                                    <ul>
                                        <?php
                                        echo force_balance_tags($cus_fields['content']);
                                        ?> 
                                    </ul>
                                </div>
                                <!-- Widget Detail Services -->
                                <?php
                            }
                            $job_fields_output = ob_get_clean();
                            echo apply_filters('jobsearch_job_detail_content_fields', $job_fields_output, $job_id);
                            ?>
                            <div class="widget jobsearch_widget_map">
                                <?php jobsearch_google_map_with_directions($job_id); ?>
                            </div>

                            <?php
                            $send_message_form_rand = rand(1000, 99999);
                            ?>
                            <div class="widget widget_contact_form">
                                <form method="post" id="jobsearch_send_message_form<?php echo esc_html($send_message_form_rand); ?>">
                                    <div class="careerfy-widget-title"><h2><?php echo esc_html__('Contact Form', 'wp-jobsearch'); ?></h2></div>
                                    <ul>
                                        <li>
                                            <input placeholder="<?php echo esc_html__('Subject', 'wp-jobsearch'); ?>" type="text" name="send_message_subject" value="">
                                            <i class="careerfy-icon careerfy-user"></i>
                                        </li>
                                        <li>
                                            <textarea placeholder="<?php echo esc_html__('Message', 'wp-jobsearch'); ?>" name="send_message_content"></textarea>
                                            <i class="careerfy-icon careerfy-mail"></i>
                                        </li>
                                        <li>
                                            <div class="input-field-submit">
                                                <input type="submit" class="send-message-submit-btn" data-action="jobsearch_job_send_message_employer" data-randid="<?php echo esc_html($send_message_form_rand); ?>" name="send_message_content" value="Enviar"/>
                                                
                                            </div> 
                                            <div class="message-box message-box-<?php echo esc_html($send_message_form_rand); ?>"></div> 
                                            <input type="hidden" name="send_message_job_id" value="<?php echo absint($job_id); ?>" />
                                        </li>
                                        <li> <img src="extra-images/widget-contact-captcha.jpg" alt=""> </li>
                                    </ul>
                                </form>
                            </div>

                        </div>
                    </aside>
                    <!-- Job Detail SideBar -->

                    <div class="careerfy-column-12">
                        <?php
                        $related_job_html = jobsearch_job_related_post($post_id, esc_html__('Related Jobs by Viant Timeline', 'wp-jobsearch'), 5, 5, '', 'view4');
                        echo $related_job_html;
                        ?>

                        <!-- Job Listings -->
                        <div class="bottom-spacer"></div>
                    </div>

                </div>
            </div>
        </div>
        <!-- Main Section -->

    </div>
    <?php
endwhile;
wp_reset_postdata();
?>
<!-- Main Content -->
<script>
    jQuery(document).on('click', '.jobsearch-sendmessage-popup-btn', function () {
        jobsearch_modal_popup_open('JobSearchModalSendMessage');
    });
    jQuery(document).on('click', '.jobsearch-sendmessage-messsage-popup-btn', function () {
        jobsearch_modal_popup_open('JobSearchModalSendMessageWarning');
    });
    jQuery(document).on('click', '.jobsearch-applyjob-msg-popup-btn', function () {
        jobsearch_modal_popup_open('JobSearchModalApplyJobWarning');
    });
</script>