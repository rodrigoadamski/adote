<?php
/**
 * Careerfy Theme functions.
 *
 * @package Careerfy
 */
if (!function_exists('careerfy_google_fonts_url')) {

    /**
     * Google Font URL
     *
     */
    function careerfy_google_fonts_url($fonts = array(), $subsets = array()) {

        /* URL */
        $base_url = "//fonts.googleapis.com/css";
        $font_args = array();
        $family = array();

        if (!empty($fonts)) {
            /* Format Each Font Family in Array */
            foreach ($fonts as $font_name => $font_weight) {
                $font_name = str_replace(' ', '+', $font_name);
                if (!empty($font_weight)) {
                    if (is_array($font_weight)) {
                        $font_weight = implode(",", $font_weight);
                    }
                    $family[] = trim($font_name . ':' . urlencode(trim($font_weight)));
                } else {
                    $family[] = trim($font_name);
                }
            }

            /* Only return URL if font family defined. */
            if (!empty($family)) {

                /* Make Font Family a String */
                $family = implode("|", $family);

                /* Add font family in args */
                $font_args['family'] = $family;

                /* Add font subsets in args */
                if (!empty($subsets)) {

                    /* format subsets to string */
                    if (is_array($subsets)) {
                        $subsets = implode(',', $subsets);
                    }

                    $font_args['subset'] = urlencode(trim($subsets));
                }

                return add_query_arg($font_args, $base_url);
            }
        }

        return '';
    }

}

if (!function_exists('careerfy_excerpt')) {

    /*
     * Custom excerpt.
     * @return content
     */

    function careerfy_excerpt($length = '', $read_more = false, $cont = false, $id = '') {
        $excerpt = get_the_excerpt();
        if ('' != $id) {
            $excerpt = get_the_excerpt($id);
        }
        if (true === $cont) {
            if ('' == $id) {
                $excerpt = get_the_content();
            } else {
                $excerpt = get_post_field('post_content', $id);
            }
        }
        if ($length > 0) {
            $excerpt = wp_trim_words($excerpt, $length, '...');
        }

        if ($read_more) {
            $excerpt .= '<a class="careerfy-readmore-btn careerfy-color" href="' . esc_url(get_permalink(get_the_ID())) . '">' . esc_html__('Read More', 'careerfy') . '</a>';
        }

        return $excerpt;
    }

}

if (!function_exists('careerfy_dynamic_sidebars')) {

    /**
     * Careerfy Dynamic Sidebars.
     * @generate sidebars
     */
    function careerfy_dynamic_sidebars() {
        global $careerfy_framework_options;

        $careerfy_sidebars = isset($careerfy_framework_options['careerfy-themes-sidebars']) ? $careerfy_framework_options['careerfy-themes-sidebars'] : '';
        if (is_array($careerfy_sidebars) && sizeof($careerfy_sidebars) > 0) {
            foreach ($careerfy_sidebars as $sidebar) {
                if ($sidebar != '') {
                    $sidebar_id = sanitize_title($sidebar);
                    register_sidebar(array(
                        'name' => $sidebar,
                        'id' => $sidebar_id,
                        'description' => esc_html__('Add widgets here.', 'careerfy'),
                        'before_widget' => '<div id="%1$s" class="widget %2$s">',
                        'after_widget' => '</div>',
                        'before_title' => '<div class="careerfy-widget-title"><h2>',
                        'after_title' => '</h2></div>',
                    ));
                }
            }
        }
    }

    add_action('widgets_init', 'careerfy_dynamic_sidebars');
}

if (!function_exists('careerfy_footer_dynamic_sidebars')) {

    /**
     * Careerfy Footer Dynamic Sidebars.
     * @generate sidebars
     */
    function careerfy_footer_dynamic_sidebars() {
        global $careerfy_framework_options;

        $footer_style = isset($careerfy_framework_options['footer-style']) ? $careerfy_framework_options['footer-style'] : '';

        $before_title = '<div class="footer-widget-title"><h2>';
        $after_title = '</h2></div>';

        if ($footer_style == 'style3') {
            $before_title = '<div class="careerfy-footer-title3"><h2>';
            $after_title = '</h2></div>';
        } else if ($footer_style == 'style4') {
            $before_title = '<div class="careerfy-footer-title4"><h2>';
            $after_title = '</h2></div>';
        }

        $before_title = apply_filters('careerfy_footer_sidebars_widget_title_before', $before_title);
        $after_title = apply_filters('careerfy_footer_sidebars_widget_title_after', $after_title);

        $careerfy_sidebars = isset($careerfy_framework_options['careerfy-footer-sidebars']) ? $careerfy_framework_options['careerfy-footer-sidebars'] : '';
        if (isset($careerfy_sidebars['col_width']) && is_array($careerfy_sidebars['col_width']) && sizeof($careerfy_sidebars['col_width']) > 0) {
            $sidebar_counter = 0;
            foreach ($careerfy_sidebars['col_width'] as $sidebar_col) {
                $sidebar = isset($careerfy_sidebars['sidebar_name'][$sidebar_counter]) ? $careerfy_sidebars['sidebar_name'][$sidebar_counter] : '';
                if ($sidebar != '') {
                    $sidebar_id = sanitize_title($sidebar);
                    register_sidebar(array(
                        'name' => $sidebar,
                        'id' => $sidebar_id,
                        'description' => esc_html__('Add only one widget here.', 'careerfy'),
                        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
                        'after_widget' => '</aside>',
                        'before_title' => $before_title,
                        'after_title' => $after_title,
                    ));
                }
                $sidebar_counter ++;
            }
        }
    }

    add_action('widgets_init', 'careerfy_footer_dynamic_sidebars');
}

if (!function_exists('careerfy_custom_str')) {

    /**
     * Careerfy Custom characters strings.
     * @return string
     */
    function careerfy_custom_str($str) {
        return $str;
    }

}

if (!function_exists('careerfy_pagination')) {

    /*
     * Pagination.
     * @return markup
     */

    function careerfy_pagination($careerfy_query = '', $return = false) {

        global $wp_query;

        $careerfy_big = 999999999; // need an unlikely integer

        $careerfy_cus_query = $wp_query;

        if (!empty($careerfy_query)) {
            $careerfy_cus_query = $careerfy_query;
        }

        $careerfy_pagination = paginate_links(array(
            'base' => str_replace($careerfy_big, '%#%', esc_url(get_pagenum_link($careerfy_big))),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $careerfy_cus_query->max_num_pages,
            'prev_text' => '<i class="careerfy-icon careerfy-arrows4"></i>',
            'next_text' => '<i class="careerfy-icon careerfy-arrows4"></i>',
            'type' => 'array'
        ));


        if (is_array($careerfy_pagination) && sizeof($careerfy_pagination) > 0) {
            $careerfy_html = '<div class="careerfy-pagination-blog">';
            $careerfy_html .= '<ul>';
            foreach ($careerfy_pagination as $careerfy_link) {
                $prev_item = $next_item = false;
                if (strpos($careerfy_link, 'current') !== false) {
                    $careerfy_html .= '<li class="active"><a>' . preg_replace("/[^0-9]/", "", $careerfy_link) . '</a></li>';
                } else {
                    $dom = new DOMDocument;
                    $dom->loadHTML($careerfy_link, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                    $this_anchor = $dom->getElementsByTagName('a');
                    if ($this_anchor->length > 0) {

                        $a_page_num = $this_anchor->item(0)->nodeValue;
                        if ($this_anchor instanceof DOMNodeList) {
                            foreach ($this_anchor as $this_anch) {
                                if ($this_anch->hasAttribute('class') && false !== strpos($this_anch->getAttribute('class'), 'prev')) {
                                    $a_page_class = str_replace('prev', 'a-prev', $this_anch->getAttribute('class'));
                                    $this_anch->setAttribute('class', $a_page_class);
                                    $prev_item = true;
                                }
                                if ($this_anch->hasAttribute('class') && false !== strpos($this_anch->getAttribute('class'), 'next')) {
                                    $a_page_class = str_replace('next', 'a-next', $this_anch->getAttribute('class'));
                                    $this_anch->setAttribute('class', $a_page_class);
                                    $next_item = true;
                                }
                            }
                        }
                        $careerfy_link = $dom->saveHtml($dom);
                    }
                    if ($prev_item == true) {
                        $careerfy_html .= '<li class="prev">' . $careerfy_link . '</li>';
                    } else if ($next_item == true) {
                        $careerfy_html .= '<li class="next">' . $careerfy_link . '</li>';
                    } else {
                        $careerfy_html .= '<li>' . $careerfy_link . '</li>';
                    }
                }
            }
            $careerfy_html .= '</ul>';

            $careerfy_html .= '</div>';

            if ($return === false) {
                echo ($careerfy_html);
            } else {
                return $careerfy_html;
            }
        }
    }

}

if (!function_exists('careerfy_excerpt_more') && !is_admin()) {

    /**
     * Replaces "[...]" (appended to automatically generated excerpts) with ... and a 'Read More' link.
     * @return link
     */
    function careerfy_excerpt_more($more) {

        if (post_password_required()) {
            return;
        }

        $link = '...';
        return $link;
    }

    add_filter('excerpt_more', 'careerfy_excerpt_more');
}

if (!function_exists('careerfy_next_prev_custom_links')) {

    /**
     * Next previous links for detail pages
     * @return links
     */
    function careerfy_next_prev_custom_links($post_type = 'post') {
        global $post;
        $previd = $nextid = '';
        $post_type = get_post_type($post->ID);
        $count_posts = wp_count_posts("$post_type")->publish;
        $careerfy_postlist_args = array(
            'posts_per_page' => -1,
            'order' => 'ASC',
            'post_type' => "$post_type",
        );
        $careerfy_postlist = get_posts($careerfy_postlist_args);
        $ids = array();
        foreach ($careerfy_postlist as $careerfy_thepost) {
            $ids[] = $careerfy_thepost->ID;
        }
        $thisindex = array_search($post->ID, $ids);
        if (isset($ids[$thisindex - 1])) {
            $previd = $ids[$thisindex - 1];
        }
        if (isset($ids[$thisindex + 1])) {
            $nextid = $ids[$thisindex + 1];
        }

        if (!empty($previd) || !empty($nextid)) {

            if ((isset($previd) && !empty($previd)) || (isset($nextid) && !empty($nextid))) {
                ?>
                <div class="careerfy-prenxt-post">
                    <ul>
                        <li class="careerfy-prenxt-post">
                            <?php
                            if (isset($previd) && !empty($previd)) {
                                $post_thumbnail_id = get_post_thumbnail_id($previd);
                                $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
                                $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';

                                if ($post_thumbnail_src != '') {
                                    ?>
                                    <figure><img src="<?php echo ($post_thumbnail_src) ?>" alt=""></figure>
                                    <?php
                                }
                                ?>
                                <div class="careerfy-prev-post">
                                    <h6><a href="<?php echo esc_url(get_permalink($previd)) ?>"><?php echo wp_trim_words(get_the_title($previd), 5, '...') ?></a></h6>
                                    <a href="<?php echo esc_url(get_permalink($previd)) ?>" class="careerfy-arrow-nexpre"><i class="careerfy-icon careerfy-down-arrow"></i> <?php esc_html_e('Previous Post', 'careerfy') ?></a>										
                                </div>
                                <?php
                            }
                            ?>
                        </li>
                        <li class="careerfy-next">
                            <?php
                            if (isset($nextid) && !empty($nextid)) {
                                $post_thumbnail_id = get_post_thumbnail_id($nextid);
                                $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'thumbnail');
                                $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';

                                if ($post_thumbnail_src != '') {
                                    ?>
                                    <figure><img src="<?php echo ($post_thumbnail_src) ?>" alt=""></figure>
                                    <?php
                                }
                                ?>
                                <div class="careerfy-next-post">
                                    <h6><a href="<?php echo esc_url(get_permalink($nextid)) ?>"><?php echo wp_trim_words(get_the_title($nextid), 5, '...') ?></a></h6>
                                    <a href="<?php echo esc_url(get_permalink($nextid)) ?>" class="careerfy-arrow-nexpre"><?php esc_html_e('Next Post', 'careerfy') ?> <i class="careerfy-icon careerfy-down-arrow"></i></a>
                                </div>
                                <?php
                            }
                            ?>
                        </li>
                    </ul>
                </div>
                <?php
            }
        }
    }

}

if (function_exists('careerfy_add_param_field')) {

    /**
     * adding multi dropdown param in vc
     * @return markup
     */
    careerfy_add_param_field('careerfy_multi_dropdown', 'careerfy_vc_multi_dropdown_field');

    function careerfy_vc_multi_dropdown_field($settings, $value) {
        $dropdown_class = 'wpb_vc_param_value wpb-textinput ' . esc_attr($settings['param_name']) . ' ' . esc_attr($settings['type']) . '_field';
        $dropdown_options = $settings['options'];

        if ($value != '' && !is_array($value)) {
            $value = explode(',', $value);
        }

        $dropdown_html = '
		<select name="' . esc_html($settings['param_name']) . '" class="' . esc_html($dropdown_class) . '" multiple="multiple">';
        foreach ($dropdown_options as $dr_opt_key => $dr_opt_val) {
            $dropdown_html .= '<option' . (is_array($value) && in_array($dr_opt_key, $value) ? ' selected="selected"' : '') . ' value="' . esc_html($dr_opt_key) . '">' . esc_html($dr_opt_val) . '</option>';
        }
        $dropdown_html .= '	
		</select>';
        return $dropdown_html;
    }

    /**
     * adding image browse param in vc
     * @return markup
     */
    careerfy_add_param_field('careerfy_browse_img', 'careerfy_vc_image_browse_field');

    function careerfy_vc_image_browse_field($settings, $value) {
        $_class = 'wpb_vc_param_value wpb-textinput ' . esc_attr($settings['param_name']) . ' ' . esc_attr($settings['type']) . '_field';

        $id = esc_attr($settings['param_name']) . rand(1000000, 9999999);

        $image_display = $value == '' ? 'none' : 'block';

        $_html = '
        <div id="' . $id . '-box" class="careerfy-browse-med-image" style="display: ' . $image_display . ';">
            <a class="careerfy-rem-media-b" data-id="' . $id . '"><i class="fa fa-close"></i></a>
            <img id="' . $id . '-img" src="' . $value . '" alt="" />
        </div>';

        $_html .= '<input type="hidden" id="' . $id . '" class="' . esc_html($_class) . '" name="' . esc_attr($settings['param_name']) . '" value="' . $value . '" />';
        $_html .= '<input type="button" class="careerfy-upload-media careerfy-bk-btn" name="' . $id . '" value="' . __('Browse', 'careerfy') . '" />';

        return $_html;
    }

}

/**
 * @Getting child Comments
 *
 */
if (!function_exists('careerfy_comments')) {

    function careerfy_comments($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        global $wpdb;

        $GLOBALS['comment'] = $comment;
        $args['reply_text'] = '<i class="fa fa-share"></i> ' . esc_html__('Reply to this comment', 'careerfy') . '';
        $args['after'] = '';
        switch ($comment->comment_type) {
            case '' :

                $comment_time = strtotime($comment->comment_date);

                $get_author = get_comment_author();
                $author_obj = get_user_by('login', $get_author);
                ?>

                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                    <div id="comment-<?php comment_ID(); ?>" class="thumblist">
                        <ul>
                            <li>

                                <?php
                                $avatar_link = get_avatar_url($comment, array('size' => 62));
                                if (@getimagesize($avatar_link)) {
                                    $avatar_link = $avatar_link;
                                } else {
                                    $avatar_link = get_template_directory_uri() . '/images/default_avatar.jpg';
                                }
                                ?>
                                <figure><img src="<?php echo esc_url_raw($avatar_link); ?>" alt=""></figure>
                                <div class="careerfy-comment-text">

                                    <h6><a><?php comment_author(); ?></a> - <span><?php echo date_i18n(get_option('date_format'), strtotime($comment->comment_date)) ?></span></h6>

                                    <?php if ($comment->comment_approved == '0') : ?>
                                        <div class="comment-awaiting-moderation"><?php echo esc_html__('Your comment is awaiting moderation.', 'careerfy'); ?></div>
                                    <?php endif; ?>
                                    <?php comment_text(); ?>
                                    <?php
                                    if (function_exists('careerfy_time_elapsed')) {
                                        ?>
                                        <span><?php echo careerfy_time_elapsed($comment_time) ?></span>
                                        <?php
                                    }
                                    comment_reply_link(array_merge($args, array('depth' => $depth)));
                                    ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <?php
                    break;
                case 'pingback' :
                case 'trackback' :
                    ?>
                <li class="post pingback">
                    <p><?php comment_author_link(); ?><?php edit_comment_link(esc_html__('Edit Comment', 'careerfy'), ' '); ?></p>
                    <?php
                    break;
            }
        }

    }

    if (!function_exists('careerfy_password_form')) {

        function careerfy_password_form() {
            global $post;
            $label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
            $html = '<form class="post-password-form" action="' . esc_url(site_url('wp-login.php?action=postpass', 'login_post')) . '" method="post">'
                    . '<div class="careerfy-protected-content">'
                    . '<span>' . esc_html__("Password To view this protected post, enter the password below:", 'careerfy') . '</span>'
                    . '<input name="post_password" id="' . esc_html($label) . '" type="password" size="20" maxlength="20" />'
                    . '<input type="submit" name="Submit" value="' . esc_attr__("Submit", 'careerfy') . '" />'
                    . '</div></form>';
            return $html;
        }

        add_filter('the_password_form', 'careerfy_password_form');
    }

    if (!function_exists('careerfy_post_detail_author_info')) {

        add_action('careerfy_post_detail_author_info', 'careerfy_post_detail_author_info');

        function careerfy_post_detail_author_info() {

            global $post;
            $user_id = get_the_author_meta('ID');

            $avatar_link = get_avatar_url(get_the_author_meta('ID'), array('size' => 114));
            if (@getimagesize($avatar_link)) {
                $avatar_link = $avatar_link;
            } else {
                $avatar_link = get_template_directory_uri() . '/images/default_avatar.jpg';
            }
            $author_box_class = 'no-post-thumbnail';
            if (has_post_thumbnail()) {
                $author_box_class = '';
            }
            ?>
            <div class="careerfy-author-detail <?php echo sanitize_html_class($author_box_class) ?>">
                <div class="detail-title"><h2><?php esc_html_e('About Author', 'careerfy') ?></h2></div>
                <figure>
                    <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>"><img src="<?php echo esc_url_raw($avatar_link); ?>" alt=""></a>
                </figure>
                <div class="detail-content">
                    <div class="post-by"><?php esc_html_e('By', 'careerfy') ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>"><?php echo get_the_author() ?></a></div>
                    <time datetime="<?php echo date('Y-m-d', strtotime(get_the_date())) ?>"><?php echo get_the_date() ?></time>
                    <?php do_action('careerfy_post_acts_btns', $post->ID) ?>
                </div>
                <?php do_action('careerfy_post_author_social_links', $post->ID) ?>
            </div>
            <?php
        }

    }

    if (!function_exists('careerfy_footer_copyright_translation')) {

        add_action('init', 'careerfy_footer_copyright_translation');

        function careerfy_footer_copyright_translation() {
            global $careerfy_framework_options;

            $careerfy_copyright_txt = isset($careerfy_framework_options['careerfy-footer-copyright-text']) ? $careerfy_framework_options['careerfy-footer-copyright-text'] : '';
            if (!empty($careerfy_copyright_txt)) {
                do_action('wpml_register_single_string', 'Careerfy Options', 'Copyright Text - ' . $careerfy_copyright_txt, $careerfy_copyright_txt);
            }
        }

    }
    if (!function_exists('advisor_special_fun')) {

        function advisor_special_fun($data = '') {
            return $data;
        }

    }