<?php

/**
 * Careerfy Theme Dynamic Colors.
 *
 * @package Careerfy
 */
function careerfy_dynamic_colors() {
    $careerfy__options = careerfy_framework_options();

    $careerfy_theme_color = isset($careerfy__options['careerfy-main-color']) && $careerfy__options['careerfy-main-color'] != '' ? $careerfy__options['careerfy-main-color'] : '#13b5ea';
    ob_start();
    ?>


    /* Plugin ThemeColor */
    .jobsearch-color,.jobsearch-colorhover:hover,.widget_nav_manu ul li:hover a,.widget_nav_manu ul li:hover a:before,
    .categories-list ul li i,li:hover .jobsearch-blog-grid-text h2 a,li:hover .jobsearch-read-more,.jobsearch-filterable ul li:hover a,.jobsearch-job-tag a,.jobsearch-list-option ul li a:hover,.jobsearch-jobdetail-postinfo,
    .jobsearch-jobdetail-options li i,.jobsearch-jobdetail-btn,.jobsearch-jobdetail-services i,.jobsearch-list-style-icon li i,.widget_view_jobs_btn,.jobsearch-employer-list small,.jobsearch-employer-list-btn,
    .jobsearch-employer-grid figcaption small,.jobsearch-fileUpload span,.jobsearch-managejobs-appli,.jobsearch-transactions-tbody small,.jobsearch-resumes-subtitle a,.jobsearch-employer-confitmation a,
    .jobsearch-candidate-default-text h2 i,.jobsearch-candidate-default-btn,.jobsearch-candidate-grid figure figcaption p a,.jobsearch_candidate_info p a,.jobsearch-candidate-download-btn,.show-toggle-filter-list,
    .jobsearch-employer-dashboard-nav ul li:hover a,.jobsearch-employer-dashboard-nav ul li.active a,.jobsearch-candidate-savedjobs tbody td span,.jobsearch-cvupload-file span,.jobsearch-modal .modal-close:hover,
    .jobsearch-box-title span,.jobsearch-user-form-info p a,.jobsearch-services-classic span i,.widget_faq ul li:hover a,.grab-classic-priceplane ul li.active i,.jobsearch-classic-priceplane.active .jobsearch-classic-priceplane-btn,
    .jobsearch-plain-services i,.jobsearch-packages-priceplane ul li i,.jobsearch-additional-priceplane-btn,.contact-service i,.jobsearch-filterable ul li:hover a i,.jobsearch-login-box form a:hover,.jobsearch-resume-addbtn:hover,.img-icons a,.jobsearch-description ul li:before,.jobsearch-employer-list small a,.jobsearch-employer-wrap-section .jobsearch-candidate-grid figure figcaption span,.jobsearch-load-more a,.jobsearch-jobdetail-btn:hover,.tabs-list li:hover a,
    .tabs-list li.active a,.sort-list-view a.active,.sort-list-view a:hover,.jobsearch-applied-job-btns .more-actions:hover,.jobsearch-applied-job-btns .more-actions:hover i,.jobsearch-profile-title h2,
    .jobsearch-typo-wrap .jobsearch-findmap-btn, .jobsearch-employer-profile-form .jobsearch-findmap-btn,.jobsearch-filterable-select select, #top .jobsearch-filterable-select select,.jobsearch-candidate-title i,
    .jobsearch-candidate-timeline-text span,.jobsearch-candidate-timeline small,.jobsearch_candidate_info small,.careerfy-employer-grid-btn,.jobsearch-employerdetail-btn,.jobsearch-typo-wrap .main-tab-section .jobsearch-employer-profile-submit:hover,.jobsearch-applied-jobs-text span,.jobsearch-employer-dashboard-nav ul li:hover i,.jobsearch-employer-dashboard-nav ul li.active i,.jobsearch-applied-job-btns .more-actions.open-options,
    .jobsearch-applied-job-btns .more-actions.open-options i,.restrict-candidate-sec a:hover,.skills-perc small,.get-skill-detail-btn:hover,.percent-num,.jobsearch-filterable-select .selectize-control.single .selectize-input input,
    .jobsearch-filterable-select .item,.jobsearch-list-option ul li.job-company-name a,.adv-srch-toggler a,.post-secin a,.jobsearch-banner-search ul li.jobsearch-banner-submit:hover i,.show-all-results a:hover,
    .jobsearch-typo-wrap .jobsearch-add-review-con input[type="submit"]:hover,.careerfy-contact-form input[type="submit"]:hover,.jobsearch-user-form input[type="submit"]:hover,
    .jobsearch-employer-profile-submit:hover,.widget_contact_form input[type="submit"]:hover,.careerfy-company-name a,.careerfy-joblisting-wrap:hover .careerfy-joblisting-text h2 a,.careerfy-more-view4-btn a:hover,
    .careerfy-banner-search-eight input[type="submit"]:hover,.careerfy-blog-view6-btn:hover,.careerfy-view7-priceplane-btn:hover,.jobsearch-subs-detail,.careerfy-candidatedetail-services ul li i {
    color: <?php echo esc_html($careerfy_theme_color) ?>;
    }
    .jobsearch-bgcolor,.jobsearch-bgcolorhover:hover,.jobsearch-banner-search ul li.jobsearch-banner-submit i,.jobsearch-plain-btn a,.jobsearch-testimonial-slider .slick-arrow:hover,
    .jobsearch-featured-label,.jobsearch-job-like:hover,.jobsearch-pagination-blog ul li:hover a, .jobsearch-pagination-blog ul li:hover span,.jobsearch-jobdetail-view,.jobsearch-jobdetail-tags a:hover,.jobsearch-employer-list-btn:hover,
    ul li:hover .jobsearch-employer-grid-btn,.widget_contact_form input[type="submit"],.jobsearch-fileUpload:hover span,.jobsearch-resumes-options li:hover a,.jobsearch-employer-jobnav ul li:hover i,.jobsearch-employer-jobnav ul li.active i,
    .jobsearch-employer-jobnav ul li.active ~ li:nth-child(2):after,.jobsearch-employer-jobnav ul li.active:nth-child(2):after,.jobsearch-employer-jobnav ul li.active:nth-child(3):after,.jobsearch-employer-confitmation a:hover,
    .jobsearch-candidate-default-btn:hover,.jobsearch-candidate-download-btn:hover,.jobsearch-add-popup input[type="submit"],.jobsearch-user-form input[type="submit"],.jobsearch-classic-services ul li:hover i,
    .jobsearch-service-slider .slick-arrow:hover,.jobsearch-classic-priceplane-btn,.jobsearch-classic-priceplane.active,.active .jobsearch-simple-priceplane-basic a,.jobsearch-packages-priceplane-btn,
    .jobsearch-additional-priceplane.active h2,.jobsearch-additional-priceplane.active .jobsearch-additional-priceplane-btn,.jobsearch-contact-info-sec,.jobsearch-contact-form input[type="submit"],.contact-service a,
    .jobsearch-employer-profile-form .jobsearch-findmap-btn:hover,.jobsearch-login-box form input[type="submit"],.jobsearch-login-box form .jobsearch-login-submit-btn, .jobsearch-login-box form .jobsearch-reset-password-submit-btn,
    .jobsearch-login-box form .jobsearch-register-submit-btn,.jobsearch-radio-checkbox input[type="radio"]:checked+label,.jobsearch-radio-checkbox input[type="radio"]:hover+label,.jobsearch-load-more a:hover,
    .jobsearch-typo-wrap .jobsearch-add-review-con input[type="submit"],.email-jobs-top,.jobalert-submit,.tabs-list li a:before,.sort-list-view a:before,.more-actions,.preview-candidate-profile:hover,
    .jobsearch-typo-wrap .ui-widget-header,.jobsearch-typo-wrap .ui-state-default, .jobsearch-typo-wrap .ui-widget-content .ui-state-default,.jobsearch-checkbox input[type="checkbox"]:checked + label span, .jobsearch-checkbox input[type="checkbox"] + label:hover span, .jobsearch-checkbox input[type="radio"]:checked + label span, .jobsearch-checkbox input[type="radio"] + label:hover span,.jobsearch-candidate-timeline small:after,
    .jobsearch_progressbar .bar,.careerfy-job-like:hover i,.jobsearch-employerdetail-btn:hover,.jobsearch-typo-wrap .jobsearch-employer-profile-submit,.sort-select-all label:after, .candidate-select-box label:after,
    .jobsearch-resume-addbtn,.jobsearch-cvupload-file:hover span,.restrict-candidate-sec a,.get-skill-detail-btn,.profile-improve-con ul li small,.complet-percent .percent-bar span,.wpcf7-form input[type="submit"],
    .jobsearch_searchloc_div .jobsearch_google_suggestions:hover,.jobsearch_searchloc_div .jobsearch_location_parent:hover,.show-all-results a,.jobsearch-jobdetail-btn.active:hover,.jobsearch-checkbox li:hover .filter-post-count,
    .careerfy-more-view4-btn a,.careerfy-banner-search-eight input[type="submit"],.careerfy-blog-view6-btn,.careerfy-view7-priceplane-btn,.jobsearch-addreview-form input[type="submit"] {
    background-color: <?php echo esc_html($careerfy_theme_color) ?>;
    }

    .jobsearch-bordercolor,.jobsearch-bordercolorhover:hover,.jobsearch-jobdetail-btn,.jobsearch-employer-list-btn,.jobsearch-fileUpload span,.jobsearch-employer-confitmation a,.jobsearch-candidate-default-btn,
    .jobsearch-candidate-download-btn,.jobsearch-cvupload-file span,.active .jobsearch-simple-priceplane-basic a,.jobsearch-additional-priceplane-btn,.jobsearch-resume-addbtn,.jobsearch-load-more a,
    .more-actions,.jobsearch-typo-wrap .ui-state-default, .jobsearch-typo-wrap .ui-widget-content .ui-state-default,.jobsearch-typo-wrap .jobsearch-findmap-btn, .jobsearch-employer-profile-form .jobsearch-findmap-btn,
    .jobsearch-checkbox input[type="checkbox"]:checked + label span, .jobsearch-checkbox input[type="checkbox"] + label:hover span, .jobsearch-checkbox input[type="radio"]:checked + label span, .jobsearch-checkbox input[type="radio"] + label:hover span,.jobsearch-jobdetail-btn.active,.jobsearch-employerdetail-btn,.jobsearch-typo-wrap .jobsearch-employer-profile-submit,.restrict-candidate-sec a,.get-skill-detail-btn,
    .jobsearch-banner-search .adv-search-options .ui-widget-content .ui-state-default,.jobsearch-banner-search ul li.jobsearch-banner-submit i,.jobsearch-typo-wrap .jobsearch-add-review-con input[type="submit"],
    .careerfy-contact-form input[type="submit"],.jobsearch-jobdetail-btn.active:hover,.jobsearch-user-form input[type="submit"]:hover,.widget_contact_form input[type="submit"],.gform_wrapper input[type="text"]:focus,
    .gform_wrapper textarea:focus,.careerfy-more-view4-btn a,.careerfy-banner-search-eight input[type="submit"] {
    border-color: <?php echo esc_html($careerfy_theme_color) ?>;
    }
    .jobsearch-read-more {
    box-shadow: 0px 0px 0px 2px <?php echo esc_html($careerfy_theme_color) ?> inset;
    }

    .jobsearch-typo-wrap button:hover, .jobsearch-typo-wrap button:focus, .jobsearch-typo-wrap input[type="button"]:hover, .jobsearch-typo-wrap input[type="button"]:focus,
    .jobsearch-typo-wrap input[type="submit"]:hover, .jobsearch-typo-wrap input[type="submit"]:focus {
    background-color: <?php echo esc_html($careerfy_theme_color) ?>;
    }


    /* ThemeColor */
    .careerfy-color,.careerfy-colorhover:hover,.widget_nav_manu ul li:hover a,.widget_nav_manu ul li:hover a:before,
    .categories-list ul li i,li:hover .careerfy-blog-grid-text h2 a,li:hover .careerfy-read-more,.careerfy-filterable ul li:hover a,.careerfy-job-tag a,.careerfy-list-option ul li a,.careerfy-jobdetail-postinfo,
    .careerfy-jobdetail-options li i,.careerfy-jobdetail-btn,.careerfy-jobdetail-services i,.careerfy-list-style-icon li i,.widget_view_jobs_btn,.careerfy-employer-list small,.careerfy-employer-list-btn,
    .careerfy-employer-grid figcaption small,.careerfy-fileUpload span,.careerfy-managejobs-appli,.careerfy-transactions-tbody small,.careerfy-resumes-subtitle a,.careerfy-employer-confitmation a,
    .careerfy-candidate-default-text h2 i,.careerfy-candidate-default-btn,.careerfy-candidate-grid figure figcaption p a,.careerfy_candidate_info p a,.careerfy-candidate-download-btn,
    .careerfy-employer-dashboard-nav ul li:hover a,.careerfy-employer-dashboard-nav ul li.active a,.careerfy-candidate-savedjobs tbody td span,.careerfy-cvupload-file span,.careerfy-modal .modal-close:hover,
    .careerfy-box-title span,.careerfy-user-form-info p a,.careerfy-services-classic span i,.widget_faq ul li:hover a,.grab-classic-priceplane ul li.active i,.careerfy-classic-priceplane.active .careerfy-classic-priceplane-btn,
    .careerfy-plain-services i,.careerfy-packages-priceplane ul li i,.careerfy-additional-priceplane-btn,.contact-service i,.careerfy-blog-author .careerfy-authorpost span,.careerfy-prev-post .careerfy-prenxt-arrow ~ a,
    .careerfy-next-post .careerfy-prenxt-arrow ~ a,.comment-reply-link,.careerfy-banner-two-btn:hover,.careerfy-banner-search-two input[type="submit"],.careerfy-fancy-title.careerfy-fancy-title-two h2 span,.careerfy-modren-btn a,.careerfy-joblisting-plain-left ul li span,.careerfy-news-grid-text ul li a,.careerfy-partnertwo-slider .slick-arrow:hover,.careerfy-testimonial-styletwo span,.careerfy-fancy-title-three i,.careerfy-testimonial-nav li:hover i,.careerfy-fancy-title-four span i,.careerfy-featured-jobs-list-text small,.careerfy-parallax-text-btn,.careerfy-footer-four .widget_section_nav ul li a:hover,.widget_footer_contact_email,.careerfy-header-option ul li:hover a,.careerfy-range-slider form input[type="submit"],.careerfy-grid-info span,.careerfy-cart-button a,.careerfy-cart-button i,.woocommerce div.product ins span,.woocommerce-review-link,.product_meta span a,.woocommerce #reviews #comments ol.commentlist li .meta time,.careerfy-shop-list .careerfy-cart-button > span,.careerfy-archive-options li a:hover,.careerfy-continue-read,.careerfy-blog-other > li i,.detail-title h2,.careerfy-author-detail .post-by a,.careerfy-continue-reading,
    .careerfy-showing-result .careerfy-post-item:hover h5 a,.careerfy-showing-result .post-author:hover a,.careerfy-classic-services i,.careerfy-accordion .panel-heading a,
    .recent-post-text .read-more-btn,.careerfy-footer-four .widget_footer_contact .widget_footer_contact_email,.jobsearch-headeight-option > li.active a:hover,.contact-service a:hover,
    .jobsearch-user-form input[type="submit"]:hover,.woocommerce .place-order button.button:hover,.jobsearch-applyjob-btn:hover,.woocommerce button.button:hover,.send-contract-to-applicnt,
    .careerfy-header-six .careerfy-headfive-option li a,.careerfy-banner-six .slick-arrow:hover,.careerfy-team-parallex span,.careerfy-blog-medium-btn,.careerfy-banner-search-seven ul li:last-child:hover i,
    .careerfy-employer-slider-btn,.careerfy-employer-slider .slick-arrow,.careerfy-candidate-view4 p,.footer-register-btn:hover,.careerfy-headseven-option > li:hover > a,.careerfy-candidate-view4 li:hover h2 a {
    color: <?php echo esc_html($careerfy_theme_color) ?>; 
    }

    .careerfy-bgcolor,.careerfy-bgcolorhover:hover,.careerfy-banner-search ul li.careerfy-banner-submit i,.careerfy-plain-btn a,.careerfy-testimonial-slider .slick-arrow:hover,
    .careerfy-featured-label,.careerfy-job-like:hover,.careerfy-pagination-blog ul li:hover a, .careerfy-pagination-blog ul li:hover span,.careerfy-jobdetail-view,.careerfy-jobdetail-tags a:hover,.careerfy-employer-list-btn:hover,
    ul li:hover .careerfy-employer-grid-btn,.widget_contact_form input[type="submit"],.careerfy-fileUpload:hover span,.careerfy-resumes-options li:hover a,.careerfy-employer-jobnav ul li:hover i,.careerfy-employer-jobnav ul li.active i,
    .careerfy-employer-jobnav ul li.active ~ li:nth-child(2):after,.careerfy-employer-jobnav ul li.active:nth-child(2):after,.careerfy-employer-jobnav ul li.active:nth-child(3):after,.careerfy-employer-confitmation a:hover,
    .careerfy-candidate-default-btn:hover,.careerfy-candidate-download-btn:hover,.careerfy-add-popup input[type="submit"],.careerfy-user-form input[type="submit"],.careerfy-classic-services ul li:hover i,
    .careerfy-service-slider .slick-arrow:hover,.careerfy-classic-priceplane-btn,.careerfy-classic-priceplane.active,.active .careerfy-simple-priceplane-basic a,.careerfy-packages-priceplane-btn,
    .careerfy-additional-priceplane.active h2,.careerfy-additional-priceplane.active .careerfy-additional-priceplane-btn,.careerfy-contact-info-sec,.careerfy-contact-form input[type="submit"],.contact-service a,
    .careerfy-tags a:hover,.widget_search input[type="submit"],.careerfy-banner-two-btn,.careerfy-banner-search-two,.careerfy-post-btn:hover,.careerfy-btn-icon,.careerfy-modren-service-link,.careerfy-modren-btn a:hover,.slick-dots li.slick-active button,.careerfy-footer-newslatter input[type="submit"],.careerfy-pagination-blog ul li.active a,.careerfy-banner-search-three input[type="submit"],.careerfy-banner-search-three input[type="submit"]:hover,.careerfy-fancy-left-title a:hover,.featured-jobs-grid-like:hover,.careerfy-services-stylethree ul li:hover span,.careerfy-priceplan-style5:hover .careerfy-priceplan-style5-btn,.active .careerfy-priceplan-style5-btn,.careerfy-banner-search-four input[type="submit"],.careerfy-parallax-text-btn:hover,.careerfy-header-option > li > a:hover,.careerfy-header-option > li.active > a,.careerfy-shop-grid figure > a:before,.careerfy-shop-grid figure > a:after,.careerfy-cart-button a:before,.careerfy-cart-button a:after,.woocommerce a.button,.woocommerce input.button,.careerfy-post-tags a:hover,.author-social-links ul li a:hover,.careerfy-static-btn,.careerfy-modren-counter ul li:after,
    .careerfy-services-classic li:hover span i,.widget_tag_cloud a:hover,.mc-input-fields input[type="submit"],.comment-respond p input[type="submit"],.jobsearch-pagination-blog ul li span.current,.careerfy-shop-label,
    .woocommerce .place-order button.button,.gform_page_footer .button,.gform_footer .gform_button.button,.careerfy-header-six .careerfy-headfive-option > li.active > a,.careerfy-banner-six-caption a,.careerfy-banner-search-six input[type="submit"],.careerfy-animate-filter ul li a.is-checked,.careerfy-services-fourtext h2:before,.careerfy-dream-packages.active .careerfy-dream-packagesplan,.careerfy-banner-search-seven ul li:last-child i,
    .careerfy-headsix-option > li:hover > a,.careerfy-headsix-option > li.active > a,.careerfy-candidate-view4-social li:hover a,.footer-register-btn,.careerfy-headseven-option > li > a {
    background-color: <?php echo esc_html($careerfy_theme_color) ?>;
    }

    .careerfy-bordercolor,.careerfy-bordercolorhover:hover,.careerfy-jobdetail-btn,.careerfy-employer-list-btn,.careerfy-fileUpload span,.careerfy-employer-confitmation a,.careerfy-candidate-default-btn,
    .careerfy-candidate-download-btn,.careerfy-cvupload-file span,.active .careerfy-simple-priceplane-basic a,.careerfy-additional-priceplane-btn,blockquote,.careerfy-banner-two-btn,.careerfy-post-btn,.careerfy-parallax-text-btn,
    .careerfy-cart-button a,.careerfy-classic-services i,.jobsearch-headeight-option > li.active > a,.contact-service a,.jobsearch-user-form input[type="submit"],.woocommerce .place-order button.button,.woocommerce button.button,
    .careerfy-header-six,.careerfy-banner-six .slick-arrow:hover,.careerfy-banner-search-seven ul li:last-child i,.careerfy-headsix-option li a,.footer-register-btn,.careerfy-headseven-option > li > a {
    border-color: <?php echo esc_html($careerfy_theme_color) ?>;
    }
    .careerfy-read-more {
    box-shadow: 0px 0px 0px 2px <?php echo esc_html($careerfy_theme_color) ?> inset;
    }

    <?php
    // header colors
    $header_bg_color = isset($careerfy__options['header-bg-color']) && $careerfy__options['header-bg-color'] != '' ? $careerfy__options['header-bg-color'] : '';
    $menu_link_color = isset($careerfy__options['menu-link-color']) && $careerfy__options['menu-link-color'] != '' ? $careerfy__options['menu-link-color'] : '';

    $submenu_bg_color = isset($careerfy__options['submenu-bg-color']) && $careerfy__options['submenu-bg-color'] != '' ? $careerfy__options['submenu-bg-color'] : '';
    $submenu_border_color = isset($careerfy__options['submenu-border-color']) && $careerfy__options['submenu-border-color'] != '' ? $careerfy__options['submenu-border-color'] : '';
    $submenu_link_color = isset($careerfy__options['submenu-link-color']) && $careerfy__options['submenu-link-color'] != '' ? $careerfy__options['submenu-link-color'] : '';
    $submenu_link_bg_color = isset($careerfy__options['submenu-link-bg-color']) && $careerfy__options['submenu-link-bg-color'] != '' ? $careerfy__options['submenu-link-bg-color'] : '';
	
    // Body background Color
    $body_background_color = isset($careerfy__options['careerfy-body-color']) && $careerfy__options['careerfy-body-color'] != '' ? $careerfy__options['careerfy-body-color'] : '';

    // footer colors

    $footer_bg_color = isset($careerfy__options['footer-bg-color']) && $careerfy__options['footer-bg-color'] != '' ? $careerfy__options['footer-bg-color'] : '';
    $footer_text_color = isset($careerfy__options['footer-text-color']) && $careerfy__options['footer-text-color'] != '' ? $careerfy__options['footer-text-color'] : '';
    $footer_link_color = isset($careerfy__options['footer-link-color']) && $careerfy__options['footer-link-color'] != '' ? $careerfy__options['footer-link-color'] : '';
    $footer_border_color = isset($careerfy__options['footer-border-color']) && $careerfy__options['footer-border-color'] != '' ? $careerfy__options['footer-border-color'] : '';
    $footer_copyright_bgcolor = isset($careerfy__options['footer-copyright-bgcolor']) && $careerfy__options['footer-copyright-bgcolor'] != '' ? $careerfy__options['footer-copyright-bgcolor'] : '';
    $footer_copyright_color = isset($careerfy__options['footer-copyright-color']) && $careerfy__options['footer-copyright-color'] != '' ? $careerfy__options['footer-copyright-color'] : '';


    // megamenu colors
    $megamenu_text_color = isset($careerfy__options['megamenu-text-color']) && $careerfy__options['megamenu-text-color'] != '' ? $careerfy__options['megamenu-text-color'] : '';
    $megamenu_bg_color = isset($careerfy__options['megamenu-bg-color']) && $careerfy__options['megamenu-bg-color'] != '' ? $careerfy__options['megamenu-bg-color'] : '';
    $megamenu_border_color = isset($careerfy__options['megamenu-border-color']) && $careerfy__options['megamenu-border-color'] != '' ? $careerfy__options['megamenu-border-color'] : '';
    $megamenu_sublink_color = isset($careerfy__options['megamenu-sublink-color']) && $careerfy__options['megamenu-sublink-color'] != '' ? $careerfy__options['megamenu-sublink-color'] : '';


	if (!empty($careerfy__options)) {
		 /*
         * Body Background color
         */

        if (isset($body_background_color) && $body_background_color != '') {
            ?>
            body{background-color: <?php echo esc_html($body_background_color) ?>;}
            <?php
        }
		
	}

    if (!empty($careerfy__options)) {

        /*
         * megamenu Paragraph colors
         */

        if (isset($megamenu_text_color) && $megamenu_text_color != '') {
            ?>
            .careerfy-mega-text p{color: <?php echo esc_html($megamenu_text_color) ?>;}
            <?php
        }

        /*
         * megamenu background colors
         */
        if (isset($megamenu_bg_color) && $megamenu_bg_color != '') {
            ?>
            .navbar-nav > li.current-menu-item > a, .navbar-nav > li.current_page_item  > a,.navbar-nav > li.active > a{color: <?php echo esc_html($megamenu_bg_color) ?>;}
            .careerfy-megamenu {background-color: <?php echo esc_html($megamenu_bg_color) ?>;}

            <?php
        }

        /*
         * megamenu border colors
         */
        if (isset($megamenu_border_color) && $megamenu_border_color != '') {
            ?>
            .careerfy-megalist li {border-color: <?php echo esc_html($megamenu_border_color) ?>;}

            <?php
        }

        /*
         * megamenu SubLink colors
         */

        if (isset($megamenu_sublink_color['regular']) && $megamenu_sublink_color['regular'] != '') {
            ?>
            .careerfy-megalist li a  {color: <?php echo esc_html($megamenu_sublink_color['regular']) ?>;}
            <?php
        }
        if (isset($megamenu_sublink_color['hover']) && $megamenu_sublink_color['hover'] != '') {
            ?>
            .careerfy-megalist li:hover a {color: <?php echo esc_html($megamenu_sublink_color['hover']) ?>;}
              { background-color: <?php echo esc_html($megamenu_sublink_color['hover']) ?>; }
            <?php
        }
        if (isset($megamenu_sublink_color['visited']) && $megamenu_sublink_color['visited'] != '') {
            ?>
            .careerfy-megalist li:hover a:visited {color: <?php echo esc_html($megamenu_sublink_color['visited']) ?>;}
            <?php
        }
        if (isset($megamenu_sublink_color['active']) && $megamenu_sublink_color['active'] != '') {
            ?>
            .careerfy-megalist > li.current-menu-item > a, .careerfy-megalist > li.current_page_item  > a,.careerfy-megalist > li.active > a{color: <?php echo esc_html($megamenu_sublink_color['active']) ?>;}
            <?php
        }
    }



    if (!empty($careerfy__options)) {

        if ($header_bg_color != '') {
            ?>
            .careerfy-header-one, .careerfy-main-header, .careerfy-main-header .careerfy-bgcolor-three, .careerfy-main-strip:before {background-color: <?php echo esc_html($header_bg_color) ?>;}
            <?php
        }
        if (isset($menu_link_color['regular']) && $menu_link_color['regular'] != '') {
            ?>
            .navbar-nav > li > a,.navbar-default .navbar-nav > li > a  {color: <?php echo esc_html($menu_link_color['regular']) ?>;}
            <?php
        }
        if (isset($menu_link_color['hover']) && $menu_link_color['hover'] != '') {
            ?>
            .navbar-nav > li:hover > a,.navbar-nav > li.active > a {color: <?php echo esc_html($menu_link_color['hover']) ?>;}
            .navbar-nav > li:hover > a:before { background-color: <?php echo esc_html($menu_link_color['hover']) ?>; }
            <?php
        }
        if (isset($menu_link_color['visited']) && $menu_link_color['visited'] != '') {
            ?>
            .navbar-nav > li > a:visited {color: <?php echo esc_html($menu_link_color['visited']) ?>;}
            <?php
        }
        if (isset($menu_link_color['active']) && $menu_link_color['active'] != '') {
            ?>
            .navbar-nav > li.current-menu-item > a, .navbar-nav > li.current_page_item  > a,.navbar-nav > li.active > a{color: <?php echo esc_html($menu_link_color['active']) ?>;}

            <?php
        }
        if ($submenu_bg_color != '') {
            ?>
            .navbar-nav .sub-menu, .navbar-nav .children {background-color: <?php echo esc_html($submenu_bg_color) ?>;}
            <?php
        }
        if ($submenu_border_color != '') {
            ?>
            .navbar-nav .sub-menu li a, .navbar-nav .children li a {border-bottom-color: <?php echo esc_html($submenu_border_color) ?>;}
            <?php
        }
        if (isset($submenu_link_color['regular']) && $submenu_link_color['regular'] != '') {
            ?>
            .navbar-nav .sub-menu li a, .navbar-nav .children li a {color: <?php echo esc_html($submenu_link_color['regular']) ?>;}
            <?php
        }
        if (isset($submenu_link_color['hover']) && $submenu_link_color['hover'] != '') {
            ?>
            .navbar-nav .sub-menu > li:hover > a, .navbar-nav .children > li:hover > a {color: <?php echo esc_html($submenu_link_color['hover']) ?>;}
            <?php
        }
        if (isset($submenu_link_color['visited']) && $submenu_link_color['visited'] != '') {
            ?>
            .navbar-nav .sub-menu li a:visited, .navbar-nav .children li a:visited {color: <?php echo esc_html($submenu_link_color['hover']) ?>;}
            <?php
        }
        if (isset($submenu_link_color['active']) && $submenu_link_color['active'] != '') {
            ?>
            .navbar-nav .sub-menu li.current-menu-item a, .navbar-nav .children li.current-menu-item a, .careerfy-megalist li.current-menu-item a {color: <?php echo esc_html($submenu_link_color['active']) ?>;}
            <?php
        }
        if (isset($submenu_link_bg_color['hover']) && $submenu_link_bg_color['hover'] != '') {
            ?>
            .navbar-nav .sub-menu li:hover, .navbar-nav .children li:hover {background-color: <?php echo esc_html($submenu_link_bg_color['hover']) ?>;}
            <?php
        }
        if (isset($submenu_link_bg_color['active']) && $submenu_link_bg_color['active'] != '') {
            ?>
            .navbar-nav .sub-menu li.current-menu-item, .navbar-nav .children li.current-menu-item {background-color: <?php echo esc_html($submenu_link_bg_color['active']) ?>;}
            <?php
        }
        if ($footer_bg_color != '') {
            ?>
            .careerfy-footer-one,.careerfy-footer-four,.careerfy-footer-three,.careerfy-footer-two,.careerfy-footer-five,.careerfy-footer-six {background-color: <?php echo esc_html($footer_bg_color) ?>;}
            <?php
        }
        if ($footer_text_color != '') {
            ?>
            .careerfy-footer-one .text,.careerfy-footer-widget p,.careerfy-footer-widget ul li,.careerfy-footer-widget table > tbody > tr > td,
            .careerfy-footer-widget table > thead > tr > th,.careerfy-footer-widget table caption,.careerfy-footer-widget i,.careerfy-footer-widget ul li p,
            .careerfy-footer-widget time,.careerfy-footer-widget span,.careerfy-footer-widget strong,.careerfy-footer-widget .widget_contact_info a,
            .careerfy-footer-two .widget_archive ul li:before {color: <?php echo esc_html($footer_text_color) ?>;}
            <?php
        }
        if (isset($footer_link_color['regular']) && $footer_link_color['regular'] != '') {
            ?>
            .careerfy-footer-one .links,.careerfy-footer-widget a,.careerfy-footer-widget .widget_product_categories li span,.careerfy-footer-widget .widget.widget_categories ul li,
            .careerfy-footer-widget .widget.widget_archive ul li,.careerfy-footer-widget .careerfy-futurecourse li a,.careerfy-footer-three .widget_nav_menu ul li a,.careerfy-footer-one .widget_nav_menu ul li a,
            .careerfy-footer-two .widget_nav_menu ul li a,.careerfy-footer-four .widget_nav_menu ul li a { 
            color: <?php echo esc_html($footer_link_color['regular']) ?>;}
            <?php
        }
        if (isset($footer_link_color['hover']) && $footer_link_color['hover'] != '') {
            ?>
            .careerfy-footer-one .links,.careerfy-footer-widget a:hover,.careerfy-footer-one .widget_nav_menu ul li a:hover,.careerfy-footer-three .widget_nav_menu ul li a:hover,
            .careerfy-footer-four .widget_nav_menu ul li a:hover,.careerfy-footer-two .widget_nav_menu ul li:hover a {color: <?php echo esc_html($footer_link_color['hover']) ?>;}
            <?php
        }
        if (isset($footer_link_color['visited']) && $footer_link_color['visited'] != '') {
            ?>
            .careerfy-footer-one .links,.careerfy-footer-widget a:visited {color: <?php echo esc_html($footer_link_color['visited']) ?>;}
            <?php
        }
        if (isset($footer_link_color['active']) && $footer_link_color['active'] != '') {
            ?>
            .careerfy-footer-one .links,.careerfy-footer-one .widget_nav_menu ul li a:hover,.careerfy-footer-three .widget_nav_menu ul li a:hover,
            .careerfy-footer-four .widget_nav_menu ul li a:hover,.widget_nav_menu ul li.current-menu-item a {color: <?php echo esc_html($footer_link_color['active']) ?>;}
            <?php
        }
        if ($footer_border_color != '') {
            ?>
            .careerfy-footer-one .border,.careerfy-footer-widget *,.careerfy-footer-widget .woocommerce.widget *,.careerfy-footer-widget .widget_articles ul li,.careerfy-footer-four .careerfy-footer-widget,.careerfy-footer-partner,.careerfy-footer-two .widget_courses-program ul li,.careerfy-copyright,.copyright-three,.careerfy-copyright-wrap {border-color: <?php echo esc_html($footer_border_color) ?>;}

            .widget_archive ul li:before { background-color: <?php echo esc_html($footer_border_color) ?>; }
            <?php
        }
        if ($footer_copyright_bgcolor != '') {
            ?>
            .careerfy-copyright,.copyright-five {background-color: <?php echo esc_html($footer_copyright_bgcolor) ?>;}
            <?php
        }
        if ($footer_copyright_color != '') {
            ?>
            .careerfy-copyright, .careerfy-copyright p, .careerfy-copyright span,.careerfy-copyright-two p,.copyright-three p {color: <?php echo esc_html($footer_copyright_color) ?>;}
            <?php
        }
    }

    $custom_styles = ob_get_clean();
    return apply_filters('careerfy_theme_colors_styles', $custom_styles, $careerfy__options);
}
