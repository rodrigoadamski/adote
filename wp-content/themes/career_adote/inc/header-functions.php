<?php
/**
 * Careerfy Header functions.
 *
 * @package Careerfy
 */
if (!function_exists('careerfy_site_logo')) {

    /**
     * Site Logo.
     * @return markup
     */
    function careerfy_site_logo($link_class = 'careerfy-logo') {
        global $careerfy_framework_options;
        $careerfy_logo = isset($careerfy_framework_options['careerfy-site-logo']['url']) && $careerfy_framework_options['careerfy-site-logo']['url'] != '' ? $careerfy_framework_options['careerfy-site-logo']['url'] : get_template_directory_uri() . '/images/logo.png';
        $logo_width = isset($careerfy_framework_options['careerfy-logo-width']) && $careerfy_framework_options['careerfy-logo-width'] > 0 ? ' width="' . $careerfy_framework_options['careerfy-logo-width'] . '"' : '';
        $logo_height = isset($careerfy_framework_options['careerfy-logo-height']) && $careerfy_framework_options['careerfy-logo-height'] > 0 ? ' height="' . $careerfy_framework_options['careerfy-logo-height'] . '"' : '';

        echo '<a class="' . $link_class . '" title="' . get_bloginfo('name') . '" href="' . esc_url(home_url('/')) . '"><img src="' . esc_url($careerfy_logo) . '"' . $logo_width . $logo_height . ' alt="' . get_bloginfo('name') . '"></a>';
    }

}

if (!function_exists('careerfy_header_section')) {

    /**
     * Site header section.
     * @return markup
     */
    function careerfy_header_section() {
        global $careerfy_framework_options, $jobsearch_plugin_options;

        $header_style = isset($careerfy_framework_options['header-style']) ? $careerfy_framework_options['header-style'] : '';
        $header_btn_txt = isset($careerfy_framework_options['header-button-text']) ? $careerfy_framework_options['header-button-text'] : '';
        $header_btn_url = isset($careerfy_framework_options['header-button-url']) ? $careerfy_framework_options['header-button-url'] : '';
        $header_btn_page = isset($careerfy_framework_options['header-button-page']) ? $careerfy_framework_options['header-button-page'] : '';
        //$post_without_reg = isset($jobsearch_plugin_options['job-post-wout-reg']) ? $jobsearch_plugin_options['job-post-wout-reg'] : '';

        if (!( class_exists('Careerfy_MMC') && true == Careerfy_MMC::is_construction_mode_enabled(false) )) {

            if ($header_style == 'style4') {
                ?>
                <div class="container">
                    <div class="row">
                        <div class="header-tabel">
                            <div class="header-row">
                                <div class="careerfy-logo-con"> 
                                    <?php careerfy_site_logo() ?> 
                                    <div class="careerfy-nav-area-wrap">
                                    <a href="javascript:void(0);" class="nav-list-mode careerfy-nav-toogle"><img src="<?php echo get_template_directory_uri() ?>/images/nav-list-icon.png" alt=""></a>
                                    <div class="careerfy-nav-area">
                                        <?php do_action('careerfy_header_navigation') ?>
                                    </div>
                                    </div>
                                </div>
                                <?php
                                if (class_exists('Careerfy_framework')) {
                                    ?>
                                    <div class="careerfy-btns-con">
                                        <ul class="careerfy-header-option">
                                            <?php
                                            $args = array(
                                                'is_popup' => true,
                                            );
                                            do_action('jobsearch_user_account_links', $args);
                                            $btn_page_obj = '';
                                            if ($header_btn_page != '') {
                                                $btn_page_obj = get_page_by_path($header_btn_page, 'OBJECT', 'page');
                                            }
                                            if (is_object($btn_page_obj) && isset($btn_page_obj->ID)) {
                                                ob_start();
                                                ?>
                                                <li><a href="<?php echo apply_filters('careerfy_header_button_url', get_permalink($btn_page_obj->ID), $btn_page_obj->ID) ?>"><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo apply_filters('careerfy_header_button_text', get_the_title($btn_page_obj->ID), $btn_page_obj->ID) ?></a></li>
                                                <?php
                                                $btn_html = ob_get_clean();
                                                echo apply_filters('careerfy_header_button_html', $btn_html);
                                            } else {
                                                if ($header_btn_txt != '' && $header_btn_url != '') {
                                                    ?>
                                                    <li><a href="<?php echo ($header_btn_url) ?>"><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo ($header_btn_txt) ?></a></li>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </ul>
                                    </div>
                                    <?php
                                    do_action('careerfy_header_top_after_btns');
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else if ($header_style == 'style5') {
                ?>
                <div class="container">
                    <div class="row">
                        <?php careerfy_site_logo() ?>
                        <div class="careerfy-right">
                            <div class="careerfy-nav-wrap">
                                <div class="careerfy-navigation-wrap">
                                    <?php do_action('careerfy_header_navigation') ?>
                                </div>
                                <?php
                                if (class_exists('Careerfy_framework')) {
                                    ?>
                                    <ul class="careerfy-headfive-option">
                                        <?php
                                        $args = array(
                                            'is_popup' => true,
                                        );
                                        do_action('jobsearch_user_account_links', $args);
                                        $btn_page_obj = '';
                                        if ($header_btn_page != '') {
                                            $btn_page_obj = get_page_by_path($header_btn_page, 'OBJECT', 'page');
                                        }
                                        if (is_object($btn_page_obj) && isset($btn_page_obj->ID)) {
                                            ob_start();
                                            ?>
                                            <li><a href="<?php echo apply_filters('careerfy_header_button_url', get_permalink($btn_page_obj->ID), $btn_page_obj->ID) ?>"><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo apply_filters('careerfy_header_button_text', get_the_title($btn_page_obj->ID), $btn_page_obj->ID) ?></a></li>
                                            <?php
                                            $btn_html = ob_get_clean();
                                            echo apply_filters('careerfy_header_button_html', $btn_html);
                                        } else {
                                            if ($header_btn_txt != '' && $header_btn_url != '') {
                                                ?>
                                                <li><a  href="<?php echo ($header_btn_url) ?>"><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo ($header_btn_txt) ?></a></li>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </ul>
                                    <?php
                                    do_action('careerfy_header_top_after_btns');
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else if ($header_style == 'style6') {
                ?>
                <div class="container">
                    <div class="row">
                        <?php careerfy_site_logo() ?>
                        <div class="careerfy-right">
                            <div class="careerfy-navigation-wrapper">
                                <?php do_action('careerfy_header_navigation') ?>
                            </div>
                            <?php
                            if (class_exists('Careerfy_framework')) {
                                ?>
                                <ul class="careerfy-headsix-option">
                                    <?php
                                    $btn_page_obj = '';
                                    if ($header_btn_page != '') {
                                        $btn_page_obj = get_page_by_path($header_btn_page, 'OBJECT', 'page');
                                    }
                                    if (is_object($btn_page_obj) && isset($btn_page_obj->ID)) {
                                        ob_start();
                                        ?>
                                        <li><a class="careerfy-color" href="<?php echo apply_filters('careerfy_header_button_url', get_permalink($btn_page_obj->ID), $btn_page_obj->ID) ?>"><?php echo apply_filters('careerfy_header_button_text', get_the_title($btn_page_obj->ID), $btn_page_obj->ID) ?></a></li>
                                        <?php
                                        $btn_html = ob_get_clean();
                                        echo apply_filters('careerfy_header_button_html', $btn_html);
                                    } else {
                                        if ($header_btn_txt != '' && $header_btn_url != '') {
                                            ?>
                                            <li><a href="<?php echo ($header_btn_url) ?>" class="careerfy-color"><?php echo ($header_btn_txt) ?></a></li>
                                            <?php
                                        }
                                    }
                                    $args = array(
                                        'is_popup' => true,
                                    );
                                    do_action('jobsearch_user_account_links', $args);
                                    ?>
                                </ul>
                                <?php
                                do_action('careerfy_header_top_after_btns');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            } else if ($header_style == 'style7') {
                ?>
                <div class="container">
                    <div class="row">
                        <?php careerfy_site_logo() ?>
                        <div class="careerfy-right">
                            <div class="careerfy-navigation-wrap">
                                <?php do_action('careerfy_header_navigation') ?>
                            </div>
                            <?php
                            if (class_exists('Careerfy_framework')) {
                                ?>
                                <ul class="careerfy-headseven-option">
                                    <?php
                                    $btn_page_obj = '';
                                    if ($header_btn_page != '') {
                                        $btn_page_obj = get_page_by_path($header_btn_page, 'OBJECT', 'page');
                                    }

                                    if (is_user_logged_in()) {
                                        if (is_object($btn_page_obj) && isset($btn_page_obj->ID)) {
                                            ob_start();
                                            ?>
                                            <li><a class="careerfy-color careerfy-post-job" href="<?php echo apply_filters('careerfy_header_button_url', get_permalink($btn_page_obj->ID), $btn_page_obj->ID) ?>"><?php echo apply_filters('careerfy_header_button_text', get_the_title($btn_page_obj->ID), $btn_page_obj->ID) ?></a></li>
                                            <?php
                                            $btn_html = ob_get_clean();
                                            echo apply_filters('careerfy_header_button_html', $btn_html);
                                        } else {
                                            if ($header_btn_txt != '' && $header_btn_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($header_btn_url) ?>" class="careerfy-color"><?php echo ($header_btn_txt) ?></a></li>
                                                <?php
                                            }
                                        }
                                    }
                                    $args = array(
                                        'is_popup' => true,
                                    );
                                    do_action('jobsearch_user_account_links', $args);
                                    ?>
                                </ul>
                                <?php
                                do_action('careerfy_header_top_after_btns');
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            } else if ($header_style == 'style3') {
                ?>
                <div class="container">
                    <div class="row">
                        
                        <div class="header-tabel">
                            <div class="header-row">
                                <?php
if ( has_nav_menu('topbar') ) { 
    wp_nav_menu( array(
        'theme_location' => 'topbar',
        'container' => false,
        'walker' => new ftb_menu()
    ));
}
?>
                                <div class="careerfy-logo-con"> 
                                    <?php careerfy_site_logo() ?> 
                                </div>
                                <div class="careerfy-menu-con">

                                    <div class="careerfy-right">
                                        <div class="navigation-subthree"><?php do_action('careerfy_header_navigation') ?></div>
                                        <?php
                                        if (class_exists('Careerfy_framework')) {
                                            ?>
                                            <ul class="careerfy-user-log">
                                                <?php
                                                $args = array(
                                                    'is_popup' => true,
                                                );
                                                do_action('jobsearch_user_account_links', $args);
                                                ?>
                                            </ul>
                                            <?php
                                            do_action('careerfy_header_top_after_btns');
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else if ($header_style == 'style2') {
                ?>
                <div class="header-tabel">
                    <div class="header-row">
                        <div class="careerfy-logo-con"> <?php careerfy_site_logo() ?> </div>
                        <div class="careerfy-menu-con">
                            <div class="careerfy-right">
                                <?php do_action('careerfy_header_navigation') ?>
                                <?php
                                if (class_exists('Careerfy_framework')) {
                                    ?>
                                    <ul class="careerfy-user-option">
                                        <?php
                                        $btn_page_obj = '';
                                        if ($header_btn_page != '') {
                                            $btn_page_obj = get_page_by_path($header_btn_page, 'OBJECT', 'page');
                                        }
                                        if (is_object($btn_page_obj) && isset($btn_page_obj->ID)) {
                                            ob_start();
                                            ?>
                                            <li><a href="<?php echo apply_filters('careerfy_header_button_url', get_permalink($btn_page_obj->ID), $btn_page_obj->ID) ?>" class="careerfy-post-btn"><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo apply_filters('careerfy_header_button_text', get_the_title($btn_page_obj->ID), $btn_page_obj->ID) ?></a></li>
                                            <?php
                                            $btn_html = ob_get_clean();
                                            echo apply_filters('careerfy_header_button_html', $btn_html);
                                        } else {
                                            if ($header_btn_txt != '' && $header_btn_url != '') {
                                                ?>
                                                <li><a href="<?php echo ($header_btn_url) ?>" class="careerfy-post-btn"><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo ($header_btn_txt) ?></a></li>
                                                <?php
                                            }
                                        }
                                        $args = array(
                                            'is_popup' => true,
                                        );
                                        do_action('jobsearch_user_account_links', $args);
                                        ?>
                                    </ul>
                                    <?php
                                    do_action('careerfy_header_top_after_btns');
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            } else {
                ?>
                <div class="container">
                    <div class="row">
                        <div class="header-tabel">
                            <div class="header-row">
                                <?php
                                if (wp_is_mobile()) {
                                    ?>
                                    <div class="careerfy-logo-con"><?php careerfy_site_logo() ?></div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="careerfy-logo-con"> <?php careerfy_site_logo() ?> 
                                        <div class="navigation-sub">
                                            <?php do_action('careerfy_header_navigation') ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                if (class_exists('Careerfy_framework')) {
                                    ?>
                                    <div class="careerfy-btns-con">
                                        <div class="careerfy-right">
                                            <?php
                                            if (wp_is_mobile()) {
                                                ?>
                                                <div class="navigation-sub">
                                                    <?php do_action('careerfy_header_navigation') ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                            <ul class="careerfy-user-section">
                                                <?php
                                                $args = array(
                                                    'is_popup' => true,
                                                );
                                                do_action('jobsearch_user_account_links', $args);
                                                ?>
                                            </ul>
                                            <?php
                                            $btn_page_obj = '';
                                            if ($header_btn_page != '') {
                                                $btn_page_obj = get_page_by_path($header_btn_page, 'OBJECT', 'page');
                                            }
                                            if (is_object($btn_page_obj) && isset($btn_page_obj->ID)) {
                                                ob_start();
                                                ?>
                                                <a href="<?php echo apply_filters('careerfy_header_button_url', get_permalink($btn_page_obj->ID), $btn_page_obj->ID) ?>" class="careerfy-simple-btn careerfy-bgcolor"><span> <i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo apply_filters('careerfy_header_button_text', get_the_title($btn_page_obj->ID), $btn_page_obj->ID) ?></span></a>
                                                <?php
                                                $btn_html = ob_get_clean();
                                                echo apply_filters('careerfy_header_button_html', $btn_html);
                                            } else {
                                                if ($header_btn_txt != '' && $header_btn_url != '') {
                                                    ?>
                                                    <a href="<?php echo ($header_btn_url) ?>" class="careerfy-simple-btn careerfy-bgcolor"><span> <i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo ($header_btn_txt) ?></span></a>
                                                    <?php
                                                }
                                            }

                                            do_action('careerfy_header_top_after_btns');
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
        }
    }

    add_action('careerfy_header_section', 'careerfy_header_section', 10);
}

if (!function_exists('careerfy_nav_fallback')) {

    /**
     * Site Navigation fallback.
     * @return markup
     */
    function careerfy_nav_fallback() {
        $pages = wp_list_pages(array('title_li' => '', 'echo' => false));

        echo '
        <ul class="level-1 nav navbar-nav">
            ' . $pages . '
        </ul>';
    }

}

if (!function_exists('careerfy_header_navigation')) {

    /**
     * Site Navigation.
     * @return markup
     */
    function careerfy_header_navigation() {
        ?>
        <!-- Navigation -->
        <a href="#menu" class="menu-link active"><span></span></a>
        <nav id="menu" class="careerfy-navigation navbar navbar-default menu">
            <?php
            $args = array(
                'theme_location' => 'primary',
                'menu_class' => 'level-1 nav navbar-nav',
                'container_class' => '',
                'container_id' => '',
                'container' => '',
                'fallback_cb' => 'careerfy_nav_fallback',
            );
            if (class_exists('careerfy_mega_menu_walker')) {
                $args['walker'] = new careerfy_mega_menu_walker();
            }
            wp_nav_menu($args);
            ?>
        </nav>
        <!-- Navigation -->
        <?php
    }

    add_action('careerfy_header_navigation', 'careerfy_header_navigation', 10);
}

if (!function_exists('careerfy_header_class')) {

    /**
     * Header Class.
     * @return class
     */
    function careerfy_header_class() {
        global $post, $careerfy_framework_options;

        $header_style = isset($careerfy_framework_options['header-style']) ? $careerfy_framework_options['header-style'] : '';
        $header_classes = array();

        if ($header_style == 'style2') {
            $header_classes[] = 'careerfy-header-two';
        } else if ($header_style == 'style3') {
            $header_classes[] = 'careerfy-header-three';
        } else if ($header_style == 'style4') {
            $header_classes[] = 'careerfy-header-four';
        } else if ($header_style == 'style5') {
            $header_classes[] = 'careerfy-header-six';
        } else if ($header_style == 'style6') {
            $header_classes[] = 'careerfy-header-seven';
        } else if ($header_style == 'style7') {
            $header_classes[] = 'careerfy-header-eight';
        } else {
            $header_classes[] = 'careerfy-header-one';
        }

        $header_class = implode(' ', $header_classes);

        return $header_class;
    }

}

if (!function_exists('careerfy_top_menu_login_links')) {

    /**
     * Header Login Links.
     * @return html
     */
    function careerfy_top_menu_login_links($links, $register_link_view = true) {
        global $careerfy_framework_options;

        $header_style = isset($careerfy_framework_options['header-style']) ? $careerfy_framework_options['header-style'] : '';

        if ($header_style == 'style2') {
            ob_start();
            ?>
            <li><a href="javascript:void(0);" class="careerfy-btn-icon jobsearch-open-signin-tab"><i class="careerfy-icon careerfy-user"></i></a></li>
            <?php
            $links = ob_get_clean();
        } else if ($header_style == 'style3') {
            ob_start();
            ?>
            <li><a href="javascript:void(0);" class="jobsearch-open-signin-tab"><i class="careerfy-icon careerfy-login"></i> <?php esc_html_e('Login', 'careerfy') ?></a></li>
            <?php
            if ($register_link_view === true) {
                ?>
                <li><a href="javascript:void(0);" class="active jobsearch-open-register-tab"><i class="careerfy-icon careerfy-user-plus"></i> <?php esc_html_e('Register', 'careerfy') ?></a></li>
                <?php
            }
            $links = ob_get_clean();
        } else if ($header_style == 'style4') {
            ob_start();
            ?>
            <li class="active jobsearch-open-signin-tab"><a href="javascript:void(0);"><i class="careerfy-icon careerfy-user"></i> <?php esc_html_e('Sign In', 'careerfy') ?></a></li>
            <?php
            $links = ob_get_clean();
        } else if ($header_style == 'style5') {

            if (is_user_logged_in()) {
                return $links;
            }

            ob_start();
            ?>
            <li class="careerfy-color"><a href="javascript:void(0);" class="jobsearch-open-signin-tab"><i class="careerfy-icon careerfy-logout"></i> <?php echo esc_html__('Sign In', 'wp-jobsearch'); ?></a></li>
            <?php
            if ($register_link_view === true) {
                ?>
                <li class="careerfy-color active"><a href="javascript:void(0);" class="jobsearch-open-register-tab"><i class="careerfy-icon careerfy-user-plus"></i> <?php echo esc_html__('Sign Up', 'wp-jobsearch'); ?></a></li>
                <?php
            }
            $links = ob_get_clean();
        } else if ($header_style == 'style6') {

            if (is_user_logged_in()) {
                return $links;
            }
            ob_start();

            if ($register_link_view === true) {
                ?>
                <li class="active"><a href="javascript:void(0);" class="jobsearch-open-register-tab"> <?php echo esc_html__('Register / Login', 'wp-jobsearch'); ?></a></li>
                <?php
            }
            $links = ob_get_clean();
        } else if ($header_style == 'style7') {
            if (is_user_logged_in()) {
                return $links;
            }
            ob_start();
            if ($register_link_view === true) {
                ?>
                <li class="active"><a href="javascript:void(0);" class="jobsearch-open-register-tab"> <?php echo esc_html__('Post New Job', 'wp-jobsearch'); ?></a></li>
                <?php
            }
            $links = ob_get_clean();
        }
        return $links;
    }

    add_filter('jobsearch_top_login_links', 'careerfy_top_menu_login_links', 10, 2);
}
 