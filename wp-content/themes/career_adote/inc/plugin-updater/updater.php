<?php

add_filter('plugins_api', 'careerfy_plugin_info', 20, 3);

function careerfy_plugin_info($res, $action, $args) {

    // do nothing if this is not about getting plugin information
    if ($action !== 'plugin_information')
        return false;

    // do nothing if it is not our plugin	
    if ('careerfy-framework' !== $args->slug)
        return $res;

    // trying to get from cache first, to disable cache comment 18,28,29,30,32
    if (false == $remote = get_transient('careerfy_upgrade_careerfy-framework')) {

        $remote = '{
        "name" : "Careerfy Framework",
        "slug" : "careerfy-framework",
        "download_url" : "https://careerfy.net/demo/wp-content/themes/careerfy/inc/activation-plugins/careerfy-framework.zip",
        "version" : "1.0.6",
        "requires" : "4.0",
        "tested" : "4.9.4",
        "last_updated" : "2018-03-22 02:10:00",
        "upgrade_notice" : "Plugin update is available.",
        "author" : "Eyecix",
        "author_homepage" : "https://themeforest.net/user/eyecix/portfolio",
            "sections" : {
                "description" : "This is a supporting plugin for Careerfy Theme.",
                "installation" : "Upload the plugin to your site, Activate it!",
                "changelog" : "<h4>1.0.6 �  March 22, 2018</h4><ul><li>Some bugs are fixed.</li></ul>"
            }
        }';

        set_transient('careerfy_upgrade_careerfy-framework', $remote, 43200); // 12 hours cache
    }

    if ($remote) {

        $remote = json_decode($remote);
        $res = new stdClass();
        $res->name = $remote->name;
        $res->slug = 'careerfy-framework';
        $res->version = $remote->version;
        $res->tested = $remote->tested;
        $res->requires = $remote->requires;
        $res->author = '<a href="https://themeforest.net/user/eyecix/portfolio">Eyecix</a>'; // I decided to write it directly in the plugin
        $res->author_profile = 'https://themeforest.net/user/eyecix/portfolio'; // profile
        $res->download_link = $remote->download_url;
        $res->trunk = $remote->download_url;
        $res->last_updated = $remote->last_updated;
        $res->sections = array(
            'description' => $remote->sections->description, // description tab
            'installation' => $remote->sections->installation, // installation tab
            'changelog' => $remote->sections->changelog, // changelog tab
                // you can add your custom sections (tabs) here 
        );

        // in case you want the screenshots tab, use the following HTML format for its content:
        // <ol><li><a href="IMG_URL" target="_blank"><img src="IMG_URL" alt="CAPTION" /></a><p>CAPTION</p></li></ol>
        if (!empty($remote->sections->screenshots)) {
            $res->sections['screenshots'] = $remote->sections->screenshots;
        }

        $res->banners = array(
            'low' => 'https://YOUR_WEBSITE/banner-772x250.jpg',
            'high' => 'https://YOUR_WEBSITE/banner-1544x500.jpg'
        );
        return $res;
    }

    return false;
}

add_filter('site_transient_update_plugins', 'careerfy_push_update');

function careerfy_push_update($transient) {
    if (empty($transient->checked)) {
        return $transient;
    }

    // trying to get from cache first, to disable cache comment 10,20,21,22,24
    if (false == $remote = get_transient('careerfy_upgrade_careerfy-framework')) {
        
        $remote = '{
        "name" : "Careerfy Framework",
        "slug" : "careerfy-framework",
        "download_url" : "https://careerfy.net/demo/wp-content/themes/careerfy/inc/activation-plugins/careerfy-framework.zip",
        "version" : "1.0.6",
        "requires" : "4.0",
        "tested" : "4.9.4",
        "last_updated" : "2018-03-22 02:10:00",
        "upgrade_notice" : "Plugin update is available.",
        "author" : "Eyecix",
        "author_homepage" : "https://themeforest.net/user/eyecix/portfolio",
            "sections" : {
                "description" : "This is a supporting plugin for Careerfy Theme.",
                "installation" : "Upload the plugin to your site, Activate it!",
                "changelog" : "<h4>1.0.6 �  March 22, 2018</h4><ul><li>Some bugs are fixed.</li></ul>"
            }
        }';

        set_transient('careerfy_upgrade_careerfy-framework', $remote, 43200); // 12 hours cache
    }

    if ($remote) {
        $remote = json_decode($remote);

        // your installed plugin version should be on the line below! You can obtain it dynamically of course
        if ($remote && version_compare('1.0', $remote->version, '<') && version_compare($remote->requires, get_bloginfo('version'), '<')) {
            $res = new stdClass();
            $res->slug = 'careerfy-framework';
            $res->plugin = 'careerfy-framework/careerfy-framework.php'; // it could be just careerfy-framework.php if your plugin doesn't have its own directory
            $res->new_version = $remote->version;
            $res->tested = $remote->tested;
            $res->package = $remote->download_url;
            $res->url = $remote->homepage;
            $transient->response[$res->plugin] = $res;
        }
    }
    return $transient;
}

add_action('upgrader_process_complete', 'careerfy_after_update', 10, 2);

function careerfy_after_update($upgrader_object, $options) {
    if ($options['action'] == 'update' && $options['type'] === 'plugin') {
        // just clean the cache when new plugin version is installed
        delete_transient('careerfy_upgrade_careerfy-framework');
    }
}