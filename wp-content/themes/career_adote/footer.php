<?php
/**
 * The template for displaying the footer.
 *
 * @package Careerfy
 */
$careerfy__options = careerfy_framework_options();
$footer_style = isset($careerfy__options['footer-style']) ? $careerfy__options['footer-style'] : '';
?> 
<!--// Footer \\-->
<footer id="careerfy-footer" class="<?php echo careerfy_footer_class() ?>">
    <?php
    if ($footer_style != 'style5') {
        ?>
        <div class="container">
            <?php
        }
        
        do_action('careerfy_footer_top_secion');
        
        
        if (!( class_exists('Careerfy_MMC') && true == Careerfy_MMC::is_construction_mode_enabled(false) )) {
            // widget area
            do_action('careerfy_footer_upper_sec');
        }
        // copyright 
        if ($footer_style != 'style5') {
            do_action('careerfy_footer_bottom_sec');
        }
        ?>

        <?php
        if ($footer_style != 'style5') {
            ?>

        </div>
    <?php } ?>

    <?php
    if ($footer_style == 'style5') {
        do_action('careerfy_footer_bottom_sec');
    }
    ?>

</footer>
<!--// Footer \\-->
<div class="clearfix"></div>
</div>
<!--// Main Wrapper \\-->
<?php
do_action('careerfy_footer_extra_content');
wp_footer();
?>
</body>
</html>
