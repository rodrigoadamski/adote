var $ = jQuery;
$(document).ready(function () {
    'use strict';

    if (careerfy_framework_vars.is_sticky == 'on') {
        jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 170) {
                jQuery('body').addClass("careerfy-sticky-header");
            } else {
                jQuery('body').removeClass("careerfy-sticky-header");
            }
        });
    }

    if (jQuery('.word-counter').length > 0) {
        jQuery('.word-counter').countUp({
            delay: 190,
            time: 3000,
        });
    }

    if (jQuery('.careerfy_twitter_widget_wrap').length > 0) {
        jQuery('.careerfy_twitter_widget_wrap').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: false,
            prevArrow: "",
            nextArrow: "",
        });
    }


    if (jQuery('.employer-slider').length > 0) {
        jQuery('.employer-slider').slick({
            infinite: true,
            slidesToShow: 6,
            slidesToScroll: 1,
            prevArrow: "<span class='slick-arrow-left'><i class='fa fa-angle-left'></i></span>",
            nextArrow: "<span class='slick-arrow-right'><i class='fa fa-angle-right'></i></span>",
            dots: true,
            autoplay: true,
            autoplaySpeed: 2000,
            infinite: true,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }



    //*** Function Banner
    if (jQuery('.careerfy-testimonial-slider').length > 0) {
        jQuery('.careerfy-testimonial-slider').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: false,
            prevArrow: "<span class='slick-arrow-left'><i class='careerfy-icon careerfy-arrow-right-bold'></i></span>",
            nextArrow: "<span class='slick-arrow-right'><i class='careerfy-icon careerfy-arrow-right-bold'></i></span>",
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    //*** Function Services Slider
    if (jQuery('.careerfy-service-slider').length > 0) {
        jQuery('.careerfy-service-slider').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: false,
            centerMode: true,
            centerPadding: '0px',
            prevArrow: "<span class='slick-arrow-left'><i class='careerfy-icon careerfy-arrow-right-bold'></i></span>",
            nextArrow: "<span class='slick-arrow-right'><i class='careerfy-icon careerfy-arrow-right-bold'></i></span>",
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    //*** Function Partner Slider
    if (jQuery('.careerfy-partner-slider').length > 0) {
        jQuery('.careerfy-partner-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: false,
            centerMode: true,
            centerPadding: '0px',
            arrows: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    if (jQuery('.careerfy-partnertwo-slider').length > 0) {
        jQuery('.careerfy-partnertwo-slider').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: false,
            prevArrow: "<span class='slick-arrow-left'><i class='careerfy-icon careerfy-arrow-pointing-to-left'></i></span>",
            nextArrow: "<span class='slick-arrow-right'><i class='careerfy-icon careerfy-arrow-pointing-to-right'></i></span>",
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 1250,
                    settings: {
                        slidesToShow: 4,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    if (jQuery('.careerfy-testimonial-styletwo').length > 0) {
        jQuery('.careerfy-testimonial-styletwo').slick({
            slidesToShow: 2,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: true,
            prevArrow: "<span class='slick-arrow-left'><i class='careerfy-icon careerfy-right-arrow-long'></i></span>",
            nextArrow: "<span class='slick-arrow-right'><i class='careerfy-icon careerfy-right-arrow-long'></i></span>",
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    if (jQuery('.careerfy-testimonial-slider-style3').length > 0) {
        jQuery('.careerfy-testimonial-slider-style3').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: true,
            fade: true,
            adaptiveHeight: true,
            prevArrow: $('.careerfy-prev'),
            nextArrow: $('.careerfy-next'),
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
    
    if (jQuery('.careerfy-testimonial-style4').length > 0) {
        jQuery('.careerfy-testimonial-style4').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: true,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

    if (jQuery('.careerfy-partner-style3').length > 0) {
        jQuery('.careerfy-partner-style3').slick({
            slidesToShow: 6,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            dots: false,
            arrows: false,
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                    }
                },
                {
                    breakpoint: 800,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 400,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }

});

jQuery(document).on('click', '.careerfy-ct-form', function (e) {
    e.preventDefault();
    var this_id = $(this).data('id'),
            msg_form = $('#ct-form-' + this_id),
            ajax_url = msg_form.data('ajax-url'),
            msg_con = msg_form.find('.careerfy-ct-msg'),
            msg_name = msg_form.find('input[name="u_name"]'),
            msg_email = msg_form.find('input[name="u_email"]'),
            msg_subject = msg_form.find('input[name="u_subject"]'),
            msg_phone = msg_form.find('input[name="u_number"]'),
            msg_type = msg_form.find('input[name="u_type"]'),
            msg_txt = msg_form.find('textarea[name="u_msg"]'),
            error = 0;

    var email_pattern = new RegExp(/^[+a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);

    if (msg_name.val() == '') {
        error = 1;
        msg_name.css({"border": "1px solid #ff0000"});
    } else {
        msg_name.css({"border": "1px solid #d3dade"});
    }

    if (msg_subject.val() == '') {
        error = 1;
        msg_subject.css({"border": "1px solid #ff0000"});
    } else {
        msg_subject.css({"border": "1px solid #d3dade"});
    }

    if (msg_email.val() == '') {
        error = 1;
        msg_email.css({"border": "1px solid #ff0000"});
    } else {
        if (!email_pattern.test(msg_email.val())) {
            error = 1;
            msg_email.css({"border": "1px solid #ff0000"});
        } else {
            msg_email.css({"border": "1px solid #d3dade"});
        }
    }

    if (msg_txt.val() == '') {
        error = 1;
        msg_txt.css({"border": "1px solid #ff0000"});
    } else {
        msg_txt.css({"border": "1px solid #d3dade"});
    }

    if (error == 0) {
        msg_con.html('<i class="fa fa-refresh fa-spin"></i>');

        var request = $.ajax({
            url: ajax_url,
            method: "POST",
            data: {
                u_name: msg_name.val(),
                u_email: msg_email.val(),
                u_subject: msg_subject.val(),
                u_phone: msg_phone.val(),
                u_msg: msg_txt.val(),
                u_type: msg_type.val(),
                action: 'careerfy_contact_form_submit',
            },
            dataType: "json"
        });

        request.done(function (response) {
            if (typeof response.msg !== 'undefined') {
                msg_name.val('');
                msg_email.val('');
                msg_subject.val('');
                msg_phone.val('');
                msg_txt.val('');
                msg_con.html(response.msg);
            } else {
                msg_con.html(kingdom_study_vars.error_msg);
            }
        });

        request.fail(function (jqXHR, textStatus) {
            msg_con.html(kingdom_study_vars.error_msg);
        });
    }

    return false;
});

jQuery(document).on('click', '.careerfy-post-like-btn', function () {
    'use strict';
    var _this = jQuery(this);
    var this_id = _this.attr('data-id');
    var icon_class = 'fa fa-heart-o';
    var icon_fill_class = 'fa fa-heart';
    var this_loader = _this.find('i');
    var this_counter = _this.find('span');
    this_loader.attr('class', 'fa fa-refresh fa-spin');
    var request = $.ajax({
        url: careerfy_funnc_vars.ajax_url,
        method: "POST",
        data: {
            post_id: this_id,
            action: 'careerfy_post_likes_count',
        },
        dataType: "json"
    });

    request.done(function (response) {
        if (typeof response.counter !== 'undefined' && response.counter != '') {
            this_counter.html(response.counter);
        }
        _this.removeAttr('class');
        _this.find('i').attr('class', icon_fill_class);
    });

    request.fail(function (jqXHR, textStatus) {
        _this.find('i').attr('class', icon_class);
    });
});

jQuery(document).on('click', '#employer-detail2-tabs li', function () {
    jQuery('#employer-detail2-tabs > li').removeClass('active');
    jQuery(this).addClass('active');
});

jQuery(document).on('click', '.careerfy-post-dislike-btn', function () {
    'use strict';
    var _this = jQuery(this);
    var this_id = _this.attr('data-id');
    var icon_class = 'fa fa-thumbs-o-down';
    var icon_fill_class = 'fa fa-thumbs-down';
    var this_loader = _this.find('i');
    var this_counter = _this.find('span');
    this_loader.attr('class', 'fa fa-refresh fa-spin');
    var request = $.ajax({
        url: careerfy_funnc_vars.ajax_url,
        method: "POST",
        data: {
            post_id: this_id,
            action: 'careerfy_post_dislikes_count',
        },
        dataType: "json"
    });

    request.done(function (response) {
        if (typeof response.counter !== 'undefined' && response.counter != '') {
            this_counter.html(response.counter);
        }
        _this.removeAttr('class');
        _this.find('i').attr('class', icon_fill_class);
    });

    request.fail(function (jqXHR, textStatus) {
        _this.find('i').attr('class', icon_class);
    });
});