<?php
/**
 * Job Categories Shortcode
 * @return html
 */
add_shortcode('careerfy_job_categories', 'careerfy_job_categories_shortcode');

function careerfy_job_categories_shortcode($atts) {
    extract(shortcode_atts(array(
        'cats_view' => '',
        'num_cats' => '',
        'result_page' => '',
                    ), $atts));

    ob_start();
    if (class_exists('JobSearch_plugin')) {
        $cats_class = 'categories-list';
        if ($cats_view == 'view2') {
            $cats_class = 'careerfy-categories careerfy-categories-styletwo';
        } else if ($cats_view == 'view3') {
            $cats_class = 'careerfy-categories careerfy-categories-stylethree';
        } else if ($cats_view == 'view4') {
            $cats_class = 'careerfy-categories careerfy-categories-stylefive';
        }
        ?>
        <div class="<?php echo ($cats_class) ?>">
            <?php
            $all_sectors = get_terms(array(
                'taxonomy' => 'sector',
                'hide_empty' => false,
            ));
            if (!empty($all_sectors) && !is_wp_error($all_sectors)) {

                $row_class = 'class="row"';
                ?>
                <ul <?php
                if ($cats_view != 'view4') {
                    echo $row_class;
                }
                ?>>
                        <?php
                        $term_count = 1;
                        foreach ($all_sectors as $term_sector) {
                            $job_args = array(
                                'posts_per_page' => '1',
                                'post_type' => 'job',
                                'post_status' => 'publish',
                                'fields' => 'ids', // only load ids
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'sector',
                                        'field' => 'slug',
                                        'terms' => $term_sector->slug
                                    )
                                ),
                                'meta_query' => array(
                                    array(
                                        'key' => 'jobsearch_field_job_publish_date',
                                        'value' => strtotime(current_time('d-m-Y H:i:s', 1)),
                                        'compare' => '<=',
                                    ),
                                    array(
                                        'key' => 'jobsearch_field_job_expiry_date',
                                        'value' => strtotime(current_time('d-m-Y H:i:s', 1)),
                                        'compare' => '>=',
                                    ),
                                    array(
                                        'key' => 'jobsearch_field_job_status',
                                        'value' => 'approved',
                                        'compare' => '=',
                                    )
                                ),
                            );
                            $jobs_query = new WP_Query($job_args);
                            $found_jobs = $jobs_query->found_posts;
                            wp_reset_postdata();

                            $term_fields = get_term_meta($term_sector->term_id, 'careerfy_frame_cat_fields', true);
                            $term_icon = isset($term_fields['icon']) ? $term_fields['icon'] : '';
                            $term_color = isset($term_fields['color']) ? $term_fields['color'] : '';
                            $term_image = isset($term_fields['image']) ? $term_fields['image'] : '';
                            if ($cats_view == 'view4') {
                                ?>
                            <li>
                                <div class="careerfy-categories-stylefive-wrap">
                                    <?php
                                    if ($term_icon != '') {
                                        ?>
                                        <i class="<?php echo ($term_icon) ?>"<?php echo ($term_color != '' && $cats_view == 'view4' ? ' style="color: ' . $term_color . ';"' : '') ?>></i>
                                        <?php
                                    }
                                    ?>
                                    <a href="<?php echo add_query_arg(array('sector_cat' => $term_sector->slug), get_permalink($result_page)) ?>"><?php echo ($term_sector->name) ?></a>
                                    <div>
                                        </li>
                                        <?php
                                    } elseif ($cats_view == 'view3') {
                                        ?>
                                        <li class="col-md-2">
                                            <div class="careerfy-categories-stylethree-wrap" <?php echo ($term_image != '' ? 'style="background-image: url(' . $term_image . '); background-size: cover;"' : '') ?>>
                                                <span></span>
                                                <div class="careerfy-categories-stylethree-text">
                                                    <h2><a href="<?php echo add_query_arg(array('sector_cat' => $term_sector->slug), get_permalink($result_page)) ?>"><strong <?php echo ($term_color != '' ? 'style="color: ' . $term_color . ';"' : '') ?>>#</strong> <?php echo ($term_sector->name) ?></a></h2>
                                                    <small><?php printf(esc_html__('(%s Jobs)', 'careerfy-frame'), $found_jobs) ?></small>
                                                </div>
                                            </div>
                                        </li>
                                        <?php
                                    } else {
                                        ?>
                                        <li class="col-md-3">
                                            <?php
                                            if ($term_icon != '') {
                                                ?>
                                                <i class="<?php echo ($term_icon) ?>"<?php echo ($term_color != '' && $cats_view == 'view2' ? ' style="background-color: ' . $term_color . ';"' : '') ?>></i>
                                                <?php
                                            }
                                            ?>
                                            <a href="<?php echo add_query_arg(array('sector_cat' => $term_sector->slug), get_permalink($result_page)) ?>"><?php echo ($term_sector->name) ?></a>
                                            <?php
                                            if ($cats_view == 'view2') {
                                                ?>
                                                <small><?php printf(esc_html__('(%s Open Vacancies)', 'careerfy-frame'), $found_jobs) ?></small>
                                                <?php
                                            } else {
                                                ?>
                                                <span><?php printf(esc_html__('(%s Open Vacancies)', 'careerfy-frame'), $found_jobs) ?></span>
                                                <?php
                                            }
                                            ?>
                                        </li>
                                        <?php
                                    }
                                    if ($num_cats > 0 && $term_count >= $num_cats) {
                                        break;
                                    }
                                    $term_count ++;
                                }
                                ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </div>
                        <?php
                        if ($result_page > 0 && $cats_view != 'view3' && $cats_view != 'view4') {
                            ?>
                            <div class="careerfy-plain-btn"> <a href="<?php echo get_permalink($result_page) ?>"><?php esc_html_e('Browse All Sectors', 'careerfy-frame') ?></a> </div>
                            <?php
                        }
                    }

                    $html = ob_get_clean();

                    return $html;
                }
                