<?php
/**
 * Advance Search Shortcode
 * @return html
 */
add_shortcode('careerfy_advance_search', 'careerfy_advance_search_shortcode');

function careerfy_advance_search_shortcode($atts) {
    extract(shortcode_atts(array(
        'view' => '',
        'srch_title' => '',
        'srch_desc' => '',
        'srch_bg_img' => '',
        'result_page' => '',
        'btn1_txt' => '',
        'btn1_url' => '',
        'btn2_txt' => '',
        'btn2_url' => '',
        'search_title_color' => '',
        'search_paragraph_color' => '',
        'search_link_color' => '',
        'search_btn_txt_color' => '',
        'search_btn_bg_color' => '',
        'search_bg_color' => '',
        'keyword_field' => 'show',
        'location_field' => 'show',
        'category_field' => 'show',
                    ), $atts));
    $transparent_bg_color = '';
    if (isset($search_bg_color) && !empty($search_bg_color)) {
        $transparent_bg_color = 'style="background-color:' . $search_bg_color . '!important"';
    }

    // search title color
    $adv_search_title_color = '';
    if (isset($search_title_color) && !empty($search_title_color)) {
        $adv_search_title_color = ' style="color:' . $search_title_color . ' !important"';
    }
    // search paragraph color
    $adv_search_paragraph_color = '';
    if (isset($search_paragraph_color) && !empty($search_paragraph_color)) {
        $adv_search_paragraph_color = ' style="color:' . $search_paragraph_color . ' !important"';
    }
    // search link color
    $adv_search_link_color = '';
    if (isset($search_link_color) && !empty($search_link_color)) {
        $adv_search_link_color = ' style="color:' . $search_link_color . ' !important"';
    }


    // search buuton text color
    $adv_search_btn_txt_color = '';
    if (isset($search_btn_txt_color) && !empty($search_btn_txt_color)) {
        $adv_search_btn_txt_color = ' color:' . $search_btn_txt_color . ' !important;';
    }
    // search button backgroung color
    $adv_search_btn_bg_color = '';
    if (isset($search_btn_bg_color) && !empty($search_btn_bg_color)) {
        $adv_search_btn_bg_color = ' background-color:' . $search_btn_bg_color . ' !important;';
    }
    $button_style = '';
    if (!empty($adv_search_btn_txt_color) || !empty($adv_search_btn_bg_color)) {
        $button_style = ' style="' . $adv_search_btn_txt_color . $adv_search_btn_bg_color . '"';
    }
    ob_start();
    if (class_exists('JobSearch_plugin')) {
        if ($view == 'view7') {
            ?>
            <div class="careerfy-search-eight-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>
			<?php if(isset($transparent_bg_color) && ($transparent_bg_color != '')) { ?>
				<span class="careerfy-banner-transparent"<?php echo ($transparent_bg_color); ?>></span>
			  <?php } ?>
                <div class="careerfy-adv-wrap">
                    <div class="container">
                        <div class="row">
                            <?php
                            if ($srch_title != '') {
                                ?>
                                <h2<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h2>
                                <?php
                            }
                            if ($srch_desc != '') {
                                ?>
                                <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                                <?php
                            }
                            ?>
                            <div class="careerfy-banner-btn">
                                <?php
                                if ($btn1_txt != '') {
                                    ?>
                                    <a href="<?php echo ($btn1_url) ?>" class="careerfy-bgcolorhover"<?php echo ($button_style) ?>><i class="careerfy-icon careerfy-arrow-up-circular"></i> <?php echo ($btn1_txt) ?></a>
                                    <?php
                                }
                                if ($btn2_txt != '') {
                                    ?>
                                    <a href="<?php echo ($btn2_url) ?>" class="careerfy-bgcolorhover"<?php echo ($button_style) ?><?php echo ($adv_search_btn_bg_color) ?>><i class="careerfy-icon careerfy-briefcase-line"></i> <?php echo ($btn2_txt) ?></a>
                                    <?php
                                }
                                ?>
                            </div>    
                            <?php
                            if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                                ?>

                                <form class="careerfy-banner-search-eight"  method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                    <ul>
                                        <?php
                                        $all_sectors = get_terms(array(
                                            'taxonomy' => 'sector',
                                            'hide_empty' => false,
                                        ));

                                        if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                            ?>
                                            <li>
                                                <div class="careerfy-select-style">
                                                    <select name="sector_cat" class="selectize-select">
                                                        <option value=""><?php esc_html_e('Categories', 'careerfy-frame') ?></option>
                                                        <?php
                                                        foreach ($all_sectors as $term_sector) {
                                                            ?>
                                                            <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </li>
                                            <?php
                                        }

                                        if ($keyword_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('Job Title, Keywords, or company', 'careerfy-frame') ?>" name="search_title" type="text">
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li><input type="submit"  value="<?php esc_html_e("Let's Go", 'careerfy-frame') ?>"> </li>
                                    </ul>
                                </form>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>


            </div>
            <?php
        } elseif ($view == 'view6') {
            ?>
            <div class="careerfy-search-seven-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>
				<span class="careerfy-banner-transparent"<?php echo ($transparent_bg_color); ?>></span>
                <div class="careerfy-adv-wrap">
                    <div class="container">
                        <div class="row">
                            <?php
                            if ($srch_title != '') {
                                ?>
                                <h2<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h2>
                                <?php
                            }
                            if ($srch_desc != '') {
                                ?>
                                <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                                <?php
                            }
                            ?>
                            <?php
                            if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                                ?>

                                <form class="careerfy-banner-search-seven" <?php echo $transparent_bg_color; ?> method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                    <ul>
                                        <?php
                                        if ($keyword_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('Job Title, Keywords, or Phrase', 'careerfy-frame') ?>" name="search_title" type="text">
                                            </li>
                                            <?php
                                        }
                                        if ($location_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('City, State or ZIP', 'careerfy-frame') ?>" name="location" type="text">
                                                <i class="careerfy-icon careerfy-location"></i>
                                            </li>
                                            <?php
                                        }
                                        $all_sectors = get_terms(array(
                                            'taxonomy' => 'sector',
                                            'hide_empty' => false,
                                        ));

                                        if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                            ?>
                                            <li>
                                                <div class="careerfy-select-style">
                                                    <select name="sector_cat" class="selectize-select">
                                                        <option value=""><?php esc_html_e('Select Sector', 'careerfy-frame') ?></option>
                                                        <?php
                                                        foreach ($all_sectors as $term_sector) {
                                                            ?>
                                                            <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li> <i class="careerfy-icon careerfy-search"></i><input type="submit"  value=""> </li>
                                    </ul>
                                </form>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>


            </div>
            <?php
        } elseif ($view == 'view5') {
            ?>
            <div class="careerfy-search-six-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>

                <div class="careerfy-adv-wrap">
                    <div class="container">
                        <div class="row">
                            <?php
                            if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                                ?>
                                <form class="careerfy-banner-search-six" <?php echo $transparent_bg_color; ?> method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                    <?php
                                    if ($srch_title != '') {
                                        ?>
                                        <h2<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h2>
                                        <?php
                                    }
                                    if ($srch_desc != '') {
                                        ?>
                                        <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                                        <?php
                                    }
                                    ?>
                                    <ul>
                                        <?php
                                        if ($keyword_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('Job Title, Keywords, or Phrase', 'careerfy-frame') ?>" name="search_title" type="text">
                                            </li>
                                            <?php
                                        }
                                        if ($location_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('City, State or ZIP', 'careerfy-frame') ?>" name="location" type="text">
                                                <i class="careerfy-icon careerfy-location"></i>
                                            </li>
                                            <?php
                                        }
                                        $all_sectors = get_terms(array(
                                            'taxonomy' => 'sector',
                                            'hide_empty' => false,
                                        ));

                                        if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                            ?>
                                            <li>
                                                <div class="careerfy-select-style">
                                                    <select name="sector_cat" class="selectize-select">
                                                        <option value=""><?php esc_html_e('Select Sector', 'careerfy-frame') ?></option>
                                                        <?php
                                                        foreach ($all_sectors as $term_sector) {
                                                            ?>
                                                            <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li> <i class="careerfy-icon careerfy-search"></i><input type="submit"  value="<?php esc_html_e('Search Jobs', 'careerfy-frame') ?>"> </li>
                                    </ul>
                                </form>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>


            </div>
            <?php
        } elseif ($view == 'view4') {
            ?>
            <div class="careerfy-search-four-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>
                <span class="careerfy-banner-transparent"<?php echo ($transparent_bg_color); ?>></span>

                <div class="careerfy-adv-wrap">
                    <div class="container">
                        <div class="row">
                            <?php
                            if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                                ?>
                                <form class="careerfy-banner-search-four" method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                    <?php
                                    if ($srch_title != '') {
                                        ?>
                                        <h2<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h2>
                                        <?php
                                    }
                                    if ($srch_desc != '') {
                                        ?>
                                        <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                                        <?php
                                    }
                                    ?>
                                    <ul>
                                        <?php
                                        if ($keyword_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('Job Title, Keywords, or Phrase', 'careerfy-frame') ?>" name="search_title" type="text">
                                            </li>
                                            <?php
                                        }
                                        if ($location_field == 'show') {
                                            ?>
                                            <li>
                                                <input placeholder="<?php esc_html_e('City, State or ZIP', 'careerfy-frame') ?>" name="location" type="text">
                                                <i class="careerfy-icon careerfy-location"></i>
                                            </li>
                                            <?php
                                        }
                                        $all_sectors = get_terms(array(
                                            'taxonomy' => 'sector',
                                            'hide_empty' => false,
                                        ));

                                        if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                            ?>
                                            <li>
                                                <div class="careerfy-select-style">
                                                    <select name="sector_cat" class="selectize-select">
                                                        <option value=""><?php esc_html_e('Select Sector', 'careerfy-frame') ?></option>
                                                        <?php
                                                        foreach ($all_sectors as $term_sector) {
                                                            ?>
                                                            <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                            <?php
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        ?>
                                        <li> <input type="submit" value="<?php esc_html_e('Search Jobs', 'careerfy-frame') ?>"> </li>
                                    </ul>
                                </form>
                                <?php
                            }
                            $top_sectors = get_terms(array(
                                'taxonomy' => 'sector',
                                'orderby' => 'count',
                                'number' => 4,
                                'hide_empty' => true,
                            ));

                            if (!empty($top_sectors) && !is_wp_error($top_sectors)) {
                                ?>
                                <ul class="careerfy-search-categories">
                                    <li<?php echo ($adv_search_paragraph_color) ?>><?php esc_html_e('Top Sectors :', 'careerfy-frame') ?></li>
                                    <?php
                                    foreach ($top_sectors as $term_sector) {
                                        ?>
                                        <li><a href="<?php echo add_query_arg(array('sector' => $term_sector->slug), get_permalink($result_page)); ?>"<?php echo ($adv_search_link_color) ?>><?php echo ($term_sector->name) ?></a></li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>


            </div>
            <?php
        } else if ($view == 'view3') {
            ?>
            <div class="careerfy-banner-three careerfy-typo-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>
                <span class="careerfy-banner-transparent"<?php echo ($transparent_bg_color); ?>></span>
                <div class="careerfy-bannerthree-caption">
                    <div class="container">
                        <?php
                        if ($srch_title != '') {
                            ?>
                            <h1<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h1>
                            <?php
                        }
                        if ($srch_desc != '') {
                            ?>
                            <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                            <?php
                        }
                        ?>
                        <div class="clearfix"></div>
                        <?php
                        if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                            ?>
                            <form class="careerfy-banner-search-three" method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                <ul>
                                    <?php
                                    if ($keyword_field == 'show') {
                                        ?>
                                        <li>
                                            <input placeholder="<?php esc_html_e('Job Title, Keywords, or Phrase', 'careerfy-frame') ?>" name="search_title" type="text">
                                        </li>
                                        <?php
                                    }
                                    if ($location_field == 'show') {
                                        ?>
                                        <li>
                                            <input placeholder="<?php esc_html_e('City, State or ZIP', 'careerfy-frame') ?>" name="location" type="text">
                                            <i class="careerfy-icon careerfy-location"></i>
                                        </li>
                                        <?php
                                    }
                                    $all_sectors = get_terms(array(
                                        'taxonomy' => 'sector',
                                        'hide_empty' => false,
                                    ));

                                    if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                        ?>
                                        <li>
                                            <div class="careerfy-select-style">
                                                <select name="sector_cat" class="selectize-select">
                                                    <option value=""><?php esc_html_e('Select Sector', 'careerfy-frame') ?></option>
                                                    <?php
                                                    foreach ($all_sectors as $term_sector) {
                                                        ?>
                                                        <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <li> <i class="careerfy-icon careerfy-search"></i> <input type="submit" value="<?php esc_html_e('Find', 'careerfy-frame') ?>"> </li>
                                </ul>
                            </form>
                            <?php
                        }
                        if ($btn1_txt != '') {
                            ?>
                            <a href="<?php echo ($btn1_url); ?>" class="careerfy-upload-cvbtn"<?php echo ($button_style) ?>><i class="careerfy-icon careerfy-upload-arrow"></i> <?php echo ($btn1_txt); ?></a>
                            <?php
                        }
                        $top_sectors = get_terms(array(
                            'taxonomy' => 'sector',
                            'orderby' => 'count',
                            'number' => 4,
                            'hide_empty' => true,
                        ));

                        if (!empty($top_sectors) && !is_wp_error($top_sectors)) {
                            ?>
                            <ul class="careerfy-search-categories">
                                <li<?php echo ($adv_search_paragraph_color) ?>><?php esc_html_e('Top Sectors :', 'careerfy-frame') ?></li>
                                <?php
                                foreach ($top_sectors as $term_sector) {
                                    ?>
                                    <li><a href="<?php echo add_query_arg(array('sector_cat' => $term_sector->slug), get_permalink($result_page)); ?>"<?php echo ($adv_search_link_color) ?>><?php echo ($term_sector->name) ?></a></li>
                                    <?php
                                }
                                ?>
                            </ul>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        } else if ($view == 'view2') {
            ?>
            <div class="careerfy-banner-two careerfy-typo-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>
                <span class="careerfy-banner-transparent"<?php echo ($transparent_bg_color); ?>></span>
                <div class="careerfy-banner-caption">
                    <div class="container">
                        <?php
                        if ($srch_title != '') {
                            ?>
                            <h1<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h1>
                            <?php
                        }
                        if ($srch_desc != '') {
                            ?>
                            <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                            <?php
                        }
                        //
                        if ($btn1_txt != '') {
                            ?>
                            <div class="clearfix"></div>
                            <a href="<?php echo ($btn1_url); ?>" class="careerfy-banner-two-btn"<?php echo ($button_style) ?>><?php echo ($btn1_txt); ?></a>
                            <?php
                        }
                        if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                            ?>
                            <div class="clearfix"></div>
                            <form class="careerfy-banner-search-two" method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                <ul>
                                    <?php
                                    if ($keyword_field == 'show') {
                                        ?>
                                        <li>
                                            <i class="careerfy-icon careerfy-search"></i>
                                            <input placeholder="<?php esc_html_e('Job Title, Keywords, or Phrase', 'careerfy-frame') ?>" name="search_title" type="text">
                                        </li>
                                        <?php
                                    }
                                    if ($location_field == 'show') {
                                        ?>
                                        <li>
                                            <input placeholder="<?php esc_html_e('City, State or ZIP', 'careerfy-frame') ?>" name="location" type="text">
                                            <i class="careerfy-icon careerfy-location"></i>
                                        </li>
                                        <?php
                                    }
                                    $all_sectors = get_terms(array(
                                        'taxonomy' => 'sector',
                                        'hide_empty' => false,
                                    ));

                                    if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                        ?>
                                        <li>
                                            <i class="careerfy-icon careerfy-folder"></i>
                                            <div class="careerfy-select-style">
                                                <select name="sector_cat" class="selectize-select">
                                                    <option value=""><?php esc_html_e('Select Sector', 'careerfy-frame') ?></option>
                                                    <?php
                                                    foreach ($all_sectors as $term_sector) {
                                                        ?>
                                                        <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <li> <input type="submit" value="<?php esc_html_e('Search Jobs', 'careerfy-frame') ?>"></li>
                                </ul>
                            </form>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        } else {
            ?>
            <div class="careerfy-banner careerfy-typo-wrap" <?php echo ($srch_bg_img != '' ? 'style="background-image: url(\'' . $srch_bg_img . '\');"' : '') ?><?php echo ($transparent_bg_color); ?>>
                <span class="careerfy-banner-overlay-color"<?php echo ($transparent_bg_color); ?>></span>
                <div class="careerfy-banner-caption">
                    <div class="container">
                        <?php
                        if ($srch_title != '') {
                            ?>
                            <h1<?php echo ($adv_search_title_color) ?>><?php echo ($srch_title) ?></h1>
                            <?php
                        }
                        if ($srch_desc != '') {
                            ?>
                            <p<?php echo ($adv_search_paragraph_color) ?>><?php echo ($srch_desc) ?></p>
                            <?php
                        }
                        if ($keyword_field == 'show' || $location_field == 'show' || $category_field == 'show') {
                            ?>
                            <form class="careerfy-banner-search" method="get" action="<?php echo (get_permalink($result_page)); ?>">
                                <ul>
                                    <?php
                                    if ($keyword_field == 'show') {
                                        ?>
                                        <li>
                                            <input placeholder="<?php esc_html_e('Job Title, Keywords, or Phrase', 'careerfy-frame') ?>" name="search_title" type="text">
                                        </li>
                                        <?php
                                    }
                                    if ($location_field == 'show') {
                                        ?>
                                        <li>
                                            <input placeholder="<?php esc_html_e('City, State or ZIP', 'careerfy-frame') ?>" name="location" type="text">
                                            <i class="careerfy-icon careerfy-location"></i>
                                        </li>
                                        <?php
                                    }
                                    $all_sectors = get_terms(array(
                                        'taxonomy' => 'sector',
                                        'hide_empty' => false,
                                    ));

                                    if (!empty($all_sectors) && !is_wp_error($all_sectors) && $category_field == 'show') {
                                        ?>
                                        <li>
                                            <div class="careerfy-select-style">
                                                <select name="sector_cat" class="selectize-select">
                                                    <option value=""><?php esc_html_e('Select Sector', 'careerfy-frame') ?></option>
                                                    <?php
                                                    foreach ($all_sectors as $term_sector) {
                                                        ?>
                                                        <option value="<?php echo urldecode($term_sector->slug) ?>"><?php echo ($term_sector->name) ?></option>
                                                        <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <li class="careerfy-banner-submit"> <input type="submit" value=""> <i class="careerfy-icon careerfy-search"></i> </li>
                                </ul>
                            </form>
                            <?php
                        }
                        ?>
                        <div class="clearfix"></div>
                        <div class="careerfy-banner-btn">
                            <?php
                            if ($btn1_txt != '') {
                                ?>
                                <a href="<?php echo ($btn1_url) ?>" class="careerfy-bgcolorhover"<?php echo ($button_style) ?>><i class="careerfy-icon careerfy-arrow-up-circular"></i> <?php echo ($btn1_txt) ?></a>
                                <?php
                            }
                            if ($btn2_txt != '') {
                                ?>
                                <a href="<?php echo ($btn2_url) ?>" class="careerfy-bgcolorhover"<?php echo ($button_style) ?><?php echo ($adv_search_btn_bg_color) ?>><i class="careerfy-icon careerfy-briefcase-line"></i> <?php echo ($btn2_txt) ?></a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
    }
    $html = ob_get_clean();

    return $html;
}
