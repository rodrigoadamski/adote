<?php
/**
 * Blog Shortcode
 * @return html
 */
add_shortcode('careerfy_blog_shortcode', 'careerfy_blog_shortcode');

function careerfy_blog_shortcode($atts) {
    extract(shortcode_atts(array(
       'blog_cat' => '',
       'blog_view' => '',
       'blog_excerpt' => '20',
       'blog_order' => 'DESC',
       'blog_orderby' => 'date',
       'blog_pagination' => 'yes',
       'blog_per_page' => '9',
                ), $atts));
    $rand_id = rand(100000, 999999);

    $html = '';

    $blog_per_page = $blog_per_page == '' ? -1 : absint($blog_per_page);

    $blog_paged = ( get_query_var('paged') ) ? absint(get_query_var('paged')) : 1;

    $args = array(
       'post_type' => 'post',
       'posts_per_page' => $blog_per_page,
       'paged' => $blog_paged,
       'post_status' => 'publish',
       'ignore_sticky_posts' => 1,
       'order' => $blog_order,
       'orderby' => $blog_orderby,
    );

    if ($blog_cat && $blog_cat != '' && $blog_cat != '0') {
        $args['tax_query'] = array(
           array(
              'taxonomy' => 'category',
              'field' => 'slug',
              'terms' => $blog_cat,
           ),
        );
    }

    $blog_query = new WP_Query($args);

    $total_posts = $blog_query->found_posts;

    ob_start();

    if ($blog_query->have_posts()) {
        global $post;

        $blog_class = 'careerfy-blog careerfy-blog-grid';
        if ($blog_view == 'view2') {
            $blog_class = 'careerfy-blog careerfy-news-grid';
        }
        if ($blog_view == 'view3') {
            $blog_class = 'careerfy-blog careerfy-blog-medium';
        }
        if ($blog_view == 'view4') {
            $blog_class = 'careerfy-blog careerfy-blog-view6';
        }

        echo '<div class="' . $blog_class . '">';
        echo '<ul class="row">';
        while ($blog_query->have_posts()) : $blog_query->the_post();
            if ($blog_view == 'view4') {
                do_action('careerfy_news_blog_view6', $post->ID, $blog_excerpt);
            } elseif ($blog_view == 'view3') {
                do_action('careerfy_news_medium', $post->ID, $blog_excerpt);
            } elseif ($blog_view == 'view2') {
                do_action('careerfy_news_grid', $post->ID, $blog_excerpt);
            } else {
                do_action('careerfy_blog_grid', $post->ID, $blog_excerpt);
            }

        endwhile;
        echo '</ul>';
        echo '</div>';
        wp_reset_postdata();

        // pagination
        if ($blog_pagination == 'yes' && $total_posts > $blog_per_page) {
            careerfy_pagination($blog_query);
        }
    } else {
        esc_html_e("No post found.", "careerfy-frame");
    }

    $html .= ob_get_clean();

    return $html;
}

if (!function_exists('careerfy_news_blog_view6')) {

    /*
     * Blog Grid View6
     * @return html
     */

    function careerfy_news_blog_view6($post_id = '', $excerpt_length = '') {

        $post_thumbnail_id = get_post_thumbnail_id($post_id);
        $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'careerfy-view6');
        $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';
        $categories = get_the_category();
        ?>

        <li class="col-md-4">
            <figure><a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><img src="<?php echo esc_url($post_thumbnail_src); ?>" alt=""></a></figure>
            <div class="careerfy-blog-view6-text-wrap">
                <div class="careerfy-blog-view6-text">
                    <h2><a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><?php echo wp_trim_words(get_the_title(get_the_ID()), 8, '...') ?></a></h2>
                    <?php
                    if (careerfy_excerpt($excerpt_length)) {
                        ?>
                        <p><?php echo careerfy_excerpt($excerpt_length) ?></p>
                        <?php
                    }
                    ?>

                    <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>" class="careerfy-blog-view6-thumb">
                        <?php echo get_avatar(get_the_author_meta('ID'), 50); ?>
                    </a>
                      
                    <div class="careerfy-blog-view6-thumb-text">   
                        <a class="careerfy-blog-view6-title" href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>"><?php echo get_the_author() ?></a>  
                        <?php
                        $category_names = array();
                        if (!empty($categories)) {
                            foreach ($categories as $category) {
                                $category_names[] = '<a href="' . get_category_link($category->term_id) . '">' . $category->name . '</a>';
                            }
                            echo implode(' / ', $category_names);
                        }
                        ?>
                    </div>
                    <a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" class="careerfy-blog-view6-btn"><?php esc_html_e('Read More', 'careerfy-frame'); ?><i class="careerfy-blog-view6-icon"></i></a>
                </div>
            </div>
        </li>
        <?php
    }

    add_action('careerfy_news_blog_view6', 'careerfy_news_blog_view6', 10, 2);
}



if (!function_exists('careerfy_news_medium')) {

    /*
     * Blog Grid View medium
     * @return html
     */

    function careerfy_news_medium($post_id = '', $excerpt_length = '') {

        $post_thumbnail_id = get_post_thumbnail_id($post_id);
        $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'careerfy-medium');
        $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';
        $categories = get_the_category();
        ?>

        <li class="col-md-6">
            <figure><a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><img src="<?php echo esc_url($post_thumbnail_src); ?>" alt=""></a></figure>
            <div class="careerfy-blog-medium-text">
                <span><time datetime="<?php echo date('Y-m-d H:i:s', strtotime(get_the_date())) ?>"><?php echo get_the_date(); ?></time></span>
                <h2><a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><?php echo wp_trim_words(get_the_title(get_the_ID()), 8, '...') ?></a></h2>
                <?php
                if (careerfy_excerpt($excerpt_length)) {
                    ?>
                    <p><?php echo careerfy_excerpt($excerpt_length) ?></p>
                    <?php
                }
                ?>
                <a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" class="careerfy-blog-medium-btn"><?php esc_html_e('Read More', 'careerfy-frame'); ?><i class="careerfy-icon careerfy-right-arrow-long"></i></a>
                <time datetime="2008-02-14 20:00"><?php echo careerfy_time_elapsed_string(get_the_time('U')); ?></time>
                <a href="#" class="careerfy-blog-medium-thumb"><?php echo get_avatar(get_the_author_meta('ID'), 50); ?><?php echo get_the_author(); ?></a>
            </div>
        </li>
        <?php
    }

    add_action('careerfy_news_medium', 'careerfy_news_medium', 10, 2);
}



if (!function_exists('careerfy_blog_grid')) {

    /*
     * Blog Grid View
     * @return html
     */

    function careerfy_blog_grid($post_id = '', $excerpt_length = '') {

        $post_thumbnail_id = get_post_thumbnail_id($post_id);
        $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'medium');
        $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';

        $categories = get_the_category();
        ?>

        <li class="col-md-4">
            <figure>
                <?php
                if ($post_thumbnail_src != '') {
                    ?>
                    <a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><img src="<?php echo esc_url($post_thumbnail_src) ?>" alt=""></a>
                    <?php
                }
                ?>
            </figure>
            <div class="careerfy-blog-grid-text">
                <?php
                if (!empty($categories)) {
                    echo '<div class="careerfy-blog-tag"> <a href="' . esc_url(get_category_link($categories[0]->term_id)) . '"> ' . esc_html($categories[0]->name) . '</a> </div>';
                }
                ?>
                <h2><a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><?php echo wp_trim_words(get_the_title(get_the_ID()), 8, '...') ?></a></h2>
                <ul class="careerfy-blog-grid-option">
                    <li><?php esc_html_e('BY', 'careerfy-frame'); ?> <a href="<?php echo esc_url(get_author_posts_url(get_the_author_meta('ID'))) ?>" class="careerfy-color"><?php echo get_the_author() ?></a></li>
                    <li><time datetime="<?php echo date('Y-m-d H:i:s', strtotime(get_the_date())) ?>"><?php echo get_the_date(); ?></time></li>
                </ul>
                <?php
                if (careerfy_excerpt($excerpt_length)) {
                    ?>
                    <p><?php echo careerfy_excerpt($excerpt_length) ?></p>
                    <?php
                }
                ?>
                <a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" class="careerfy-read-more careerfy-bgcolor"><?php esc_html_e('Read Articles', 'careerfy-frame'); ?></a>
            </div>
        </li>
        <?php
    }

    add_action('careerfy_blog_grid', 'careerfy_blog_grid', 10, 2);
}

if (!function_exists('careerfy_news_grid')) {

    /*
     * News Grid View
     * @return html
     */

    function careerfy_news_grid($post_id = '', $excerpt_length = '') {

        $post_thumbnail_id = get_post_thumbnail_id($post_id);
        $post_thumbnail_image = wp_get_attachment_image_src($post_thumbnail_id, 'medium');
        $post_thumbnail_src = isset($post_thumbnail_image[0]) && esc_url($post_thumbnail_image[0]) != '' ? $post_thumbnail_image[0] : '';
        $categories = get_the_category();
        ?>

        <li class="col-md-4">
            <div class="careerfy-news-grid-wrap">
                <figure>
                    <?php
                    if ($post_thumbnail_src != '') {
                        ?>
                        <a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><img src="<?php echo esc_url($post_thumbnail_src) ?>" alt=""></a>
                        <?php
                    }
                    ?>
                </figure>
                <div class="careerfy-news-grid-text">
                    <ul>
                        <li><?php echo get_the_date() ?></li>
                        <?php
                        if (!empty($categories)) {
                            echo '<li><a href="' . esc_url(get_category_link($categories[0]->term_id)) . '"> ' . esc_html($categories[0]->name) . '</a></li>';
                        }
                        ?>
                    </ul>
                    <h2><a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>"><?php echo wp_trim_words(get_the_title(get_the_ID()), 8, '...') ?></a></h2>
                    <?php
                    if (careerfy_excerpt($excerpt_length)) {
                        ?>
                        <p><?php echo careerfy_excerpt($excerpt_length) ?></p>
                        <?php
                    }
                    ?>
                    <a href="<?php echo esc_url(get_permalink(get_the_ID())) ?>" class="careerfy-modren-service-link"><i class="careerfy-icon careerfy-right-arrow-long"></i></a>
                </div>
            </div>
        </li>
        <?php
    }

    add_action('careerfy_news_grid', 'careerfy_news_grid', 10, 2);
}
