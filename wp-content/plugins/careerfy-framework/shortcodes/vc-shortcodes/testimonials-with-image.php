<?php

/**
 * Testimonials Shortcode
 * @return html
 */
add_shortcode('careerfy_testimonials', 'careerfy_testimonials_shortcode');

function careerfy_testimonials_shortcode($atts, $content = '') {

    global $testi_view;
    extract(shortcode_atts(array(
       'testi_view' => '',
       'img' => '',
                ), $atts));
    wp_enqueue_script('careerfy-slick-slider');
    
    if ($testi_view == 'view4') {
        $html = '
        <div class="careerfy-testimonial-style4">
            ' . do_shortcode($content) . '
        </div>' . "\n";
    }
     else if ($testi_view == 'view3') {
        $html = '
        <div class="container-fluid">
            <div class="row">
                <div class="careerfy-testimonial-slider-style3-wrap">
                    ' . ($img != '' ? '<div class="careerfy-plan-thumb"><img src="' . $img . '" alt=""></div>' : '') . '
                    <div class="careerfy-testimonial-slider-style3">
                        ' . do_shortcode($content) . '
                    </div>
                    <ul class="careerfy-testimonial-nav">
                        <li class="careerfy-prev"><i class="careerfy-icon careerfy-right-arrow-long"></i></li>
                        <li class="careerfy-next"><i class="careerfy-icon careerfy-right-arrow-long"></i></li>
                    </ul>
                </div>
            </div>
        </div>' . "\n";
    } else if ($testi_view == 'view2') {
        $html = '
        <div class="careerfy-testimonial-styletwo">
            ' . do_shortcode($content) . '
        </div>' . "\n";
    } else {
        $html = '
        <div class="careerfy-testimonial-section">
            <div class="row">
                ' . ($img != '' ? '<aside class="col-md-5"> <img src="' . $img . '" alt=""> </aside>' : '') . '
                <aside class="col-md-7">
                    <div class="careerfy-testimonial-slider">
                        ' . do_shortcode($content) . '
                    </div>
                </aside>
            </div>
        </div>' . "\n";
    }
    return $html;
}
add_shortcode('careerfy_testimonial_item', 'careerfy_testimonial_item_shortcode');
function careerfy_testimonial_item_shortcode($atts) {

    global $testi_view;
    extract(shortcode_atts(array(
       'img' => '',
       'desc' => '',
       'title' => '',
       'position' => '',
                ), $atts));
    if ($testi_view == 'view4') {
        $html = '
        <div class="careerfy-testimonial-style4-layer">
            <img src="' . ($img) . '" alt="">
            <p>' . ($desc) . '</p>
            <span>' . ($title) . ' <small>' . ($position) . '</small> </span>
        </div>';
    }
    else if ($testi_view == 'view3') {
        $html = '
        <div class="careerfy-testimonial-slider-style3-layer">
            <div class="testimonial-slider-style3-text">
                <p>' . ($desc) . '</p>
                <span><i class="careerfy-icon careerfy-left-quote"></i> ' . ($position != '' ? '<small>' . $title . ',</small>' : '') . ' ' . ($position) . '</span>
            </div>
        </div>';
    } else if ($testi_view == 'view2') {
        $html = '
        <div class="careerfy-testimonial-styletwo-layer">
            <img src="' . ($img) . '" alt="">
            <p>' . ($desc) . '</p>
            <span>' . ($title) . '</span>
            <small>' . ($position) . '</small>
        </div>';
    } else {
        $html = '
        <div class="careerfy-testimonial-slide-layer">
            <div class="careerfy-testimonial-wrap">
                <p>' . ($desc) . '</p>
                <div class="careerfy-testimonial-text">
                    <h2>' . ($title) . '</h2>
                    <span>' . ($position) . '</span>
                </div>
            </div>
        </div>';
    }
    return $html;
}
